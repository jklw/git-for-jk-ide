This code is free, that is you may use it for free for any purpose, but you must
not make money from it. It is ok, if you use it as a tool for develloping your 
own commercial software, but you are not allowed to sell the supplied code or 
derivative work of it.


In order to use it you must adapt and recompile this project!
The resulting executable (VCS.exe) must be added as VCS Tool in Path Options Dialog, 
if you copy VCS.exe to the IDE�s directory, you don�t need to give a full path.

This is PowerBASIC code, you will need PBWIN 10 and Jos� Roca�s includes for compiling.
It shouldn�t be too hard to translate it to FreeBASIC, if someone has a need to.


After installing GIT for Windows and TortoiseGIT you MUST adapt two variables:
- gitpath = "E:\GIT\PortableGit\cmd\git.exe"
- guipath = "C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe"

gitpath must point to YOUR setting, please note that it must specify "git.exe" inside
        the "cmd" subdir!
guipath must point to YOUR setting, it doesn�t matter if you install a 32 or a 64 version        


This version adds and removes file in the working tree automatically before a commit, 
if you want to control and do it yourself you must outcomment the corresponding lines in
function "git_commit", see comments there.

Be sure to understand what you do when applying further changes, please study the code 
and the available docs for GIT and TortoiseGit. If you need help or find bugs, feel free
to drop me a mail at: jk-ide@t-online.de



How does it work:
-----------------

Everytime a dropdown menu option of the VCS toolbar button (Bottom toolbar) is clicked
and VCS.exe has been added as VCS tool in Path Option Dialog, VCS.exe is shelled.

VCS.exe gets 4 commandline parameters: 
- ide_hwnd = JK-IDE handle
- cmd_id   = command id (which menu option)
- pname    = project name
- ver      = version number (dword = 4 bytes, one byte each number)
 
a hidden window is created for exchanging messages with the IDE, The IDE waits for VCS.exe
to finish, the wait amination (a blue spinner) is visible, as long as the IDE remains in a
wait state. You can click upon the spinner to exit the wait state, in case VCS.exe doesn�t
return gracefully or doesn�t return at all.

The IDE sends a list of opened files of the current project in the IDE, the active file is 
the first in list, "f$" holds this list (full path, "|" separated).

You can (re-)open, close (without saving), or save a file in the IDE via WM_Copydata
(dwdata = %IDE_Open/%IDE_Close/%IDE_Save/%IDE_Output, buffer = filename (asciiz, case insensitive)
and you may send text to the IDE�s output window, please study the code to understand
how exactly this works. 

The more you can (and should) control the vcs open flag (lets the toolbar button appear red for 
"repository is open" among other things), and post a close message for the output window:
postmessage ide_hwnd, %WM_App + 6000, 2, value         'set/reset vcs open flag (0=reset, >0=set)
postmessage ide_hwnd, %WM_App + 6000, 3, 0             'close output window after 2.5 seconds


 
Have fun


JK 