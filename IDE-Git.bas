'***********************************************************************************************
'   ' to do:
'' test it
'asdf '/ test' /'   asdfx 'xyz
'   '/ test1 /'   asdfx1
'  '/ test2 /'   asdfx2
' '/ test3 /'   asdfx3
''/ test4 /'   asdfx4
'' make console topmost  ???
''***********************************************************************************************
'***********************************************************************************************



#compile exe "VCS.exe"
'   ods("test")
#dim all

#UTILITY roca2                                        'use Jos� Roca�s include version 2
#include "win32api.inc"
#include "Commctrl.inc"
#include "Commdlg.inc"

#RESOURCE RES, "Resources\Resource.res"


'***********************************************************************************************
'***********************************************************************************************

%VCS_Open           = 10000                           'VCS dropdown menu "Open (current Version)"
%VCS_Changes        = 10001                           'VCS dropdown menu "Changes"
%VCS_Remote         = 10002                           'VCS dropdown menu "Remote"
%VCS_Commit         = 10003                           'VCS dropdown menu "Commit"
%VCS_Commit_Option  = 10004                           'VCS dropdown menu "Commit (with Options)"
%VCS_Auto_Commit    = 10005                           'auto commit at session end
%VCS_Close          = 10006                           'VCS dropdown menu "Close"
%VCS_Restore        = 10007                           'VCS dropdown menu "Restore (previous Version)"
%VCS_GUI            = 10008                           'VCS dropdown menu "GUI"
%VCS_Console        = 10009                           'VCS dropdown menu "Console"

%IDE_Open           = 101                             '(re)open file in IDE
%IDE_Close          = 102                             'close file in IDE (without saving)
%IDE_Save           = 103                             'save file in IDE
%IDE_Output         = 104                             'send string to outputwindow


%IDC_EDIT = 101                                       'scintilla id
%IDC_OutW = 523                                       'output window id


global hinst     as dword                             'application instance handle
global hwndm     as dword                             'main window (for processing messages only)
global hwndb     as dword                             'git gui window

global gitpath   as string                            'where to find git.exe
global guipath   as string                            'where to find TortoiseGitProc.exe
global cpath     as string                            'current path (root of working tree)

global ide_hwnd  as dword                             'IDE�s handle
global cmd_id    as dword                             'command id
global pname     as string                            'project name
global ver       as dword                             'version number
global f$                                             'list of open files of this project in IDE ("|" separated)
                                                      'first file = active file
global ftw()     as quad                              'file last write time
global hw_out    as dword                             'IDE�s output window handle
global hp        as dword                             'process handle
global rflag     as dword                             'reload flag (1 = prevent reloading files) 


#include "gui.bas"


'***********************************************************************************************


union mytime
  ft as filetime
  q  as quad
end union


UNION JK_EITHERCHAR
    UnicodeChar AS WSTRING * 1
    AsciiChar   AS STRING * 1
END UNION
'
'
TYPE JK_KEY_EVENT_RECORD
    bKeyDown          AS LONG
    wRepeatCount      AS WORD
    wVirtualKeyCode   AS WORD
    wVirtualScanCode  AS WORD
    JK_EITHERCHAR
    dwControlKeyState AS DWORD
END TYPE


TYPE JK_INPUT_RECORD DWORD
  EventType AS WORD
  JK_KEY_EVENT_RECORD
END TYPE


DECLARE FUNCTION JK_WriteConsoleInput LIB "KERNEL32.DLL" _
    ALIAS "WriteConsoleInputA" (BYVAL hConsoleInput AS DWORD, _
    BYVAL lpBuffer AS JK_INPUT_RECORD PTR, BYVAL nLength AS DWORD, _
    lpNumberOfEventsWritten AS DWORD) AS LONG


FUNCTION AfxIniFileRead (BYVAL bstrIniFileName AS WSTRING, BYVAL bstrSectionName AS WSTRING, BYVAL bstrKeyName AS WSTRING, OPTIONAL BYVAL bstrDefault AS WSTRING) AS WSTRING
   LOCAL dwChars AS DWORD
   LOCAL wszReturnedString AS WSTRINGZ * 65535
   dwChars = GetPrivateProfileStringW(BYCOPY bstrSectionName, BYCOPY bstrKeyName, BYCOPY bstrDefault, wszReturnedString, SIZEOF(wszReturnedString), BYCOPY bstrIniFileName)
   FUNCTION = LEFT$(wszReturnedString, dwChars)
END FUNCTION


FUNCTION AfxIniFileWrite (BYVAL bstrIniFileName AS WSTRING, BYVAL bstrSectionName AS WSTRING, BYVAL bstrKeyName AS WSTRING, BYVAL bstrValue AS WSTRING) AS LONG
   FUNCTION = WritePrivateProfileStringW(BYCOPY bstrSectionName, BYCOPY bstrKeyName, BYCOPY bstrValue, BYCOPY bstrIniFileName)
END FUNCTION


'***********************************************************************************************
' WM_Copydata      dwdata = %IDE_Open/%IDE_Close/%IDE_Save/%IDE_Output
'                  buffer = filename (asciiz, case insensitive)
'
' postmessage ide_hwnd, %WM_App + 6000, 1, hwnd          'request for file list
' postmessage ide_hwnd, %WM_App + 6000, 2, value         'set/reset vcs open flag (0=reset, >0=set)
' postmessage ide_hwnd, %WM_App + 6000, 3, 0             'close output window after 2.5 seconds
'***********************************************************************************************


%vcs_log = 0                                          '1 = debug output to debug string viewer


global git_handle as dword                            'git process handle


#if %vcs_log = 1                                      'output debug messages to debug string viewer
  macro gol(outtext)
    ods(outtext)
  end macro
#else
  macro gol(outtext)
  end macro
#endif


'***********************************************************************************************


FUNCTION MonitorEnumProc(BYVAL hmonitor AS DWORD, BYVAL hdc AS DWORD, BYVAL r AS rect PTR, BYVAL dummy AS DWORD) AS LONG
'***********************************************************************************************
' monitor enum callback
'***********************************************************************************************
LOCAL rp AS rect PTR
LOCAL mi AS monitorinfo


  mi.cbsize=SIZEOF(monitorinfo)                       'setup type
  CALL getmonitorinfo(hmonitor, mi)                   'check for primary monitor


  rp = dummy                                          'pass pointer

  IF (@rp.nleft => @r.nleft) AND (@rp.nleft <= @r.nright) THEN
    IF (@rp.ntop => @r.ntop) AND (@rp.ntop <= @r.nbottom) THEN

      @rp = @r                                        'pass content
                                                     
      FUNCTION=0                                      'stop enumerating
      EXIT FUNCTION
    END IF
  END IF


  FUNCTION = -1                                       'keep on enumerating


END FUNCTION


'***********************************************************************************************


sub jk_CenterWindow(BYVAL hwnd AS DWORD, BYVAL hwndParent AS DWORD)
'***********************************************************************************************
' center in parent
'***********************************************************************************************
LOCAL rc            AS RECT                           ' Window coordinates
LOCAL nWidth        AS LONG                           ' Width of the window
LOCAL nHeight       AS LONG                           ' Height of the window
LOCAL rcParent      AS RECT                           ' Parent window coordinates
LOCAL nParentWidth  AS LONG                           ' Width of the parent window
LOCAL nParentHeight AS LONG                           ' Height of the parent window
LOCAL rcWorkArea    AS RECT                           ' Work area coordinates
LOCAL pt            AS POINT                          ' x and y coordinates of centered window


  IF hwnd = 0 THEN exit sub
  GetWindowRect hwnd, rc

  nWidth = rc.Right - rc.Left                         'Calculate the width and height
  nHeight = rc.Bottom - rc.Top


  IF hwndParent = 0 then
    exit sub
  else
     GetWindowRect hwndParent, rcParent               'Get the coordinates of the parent window
  END IF

  nParentWidth = rcParent.nRight - rcParent.nLeft     'Calculate the width and height of the parent window
  nParentHeight = rcParent.nBottom - rcParent.nTop


  rcWorkArea.nLeft = (rcParent.nleft + rcParent.nright)/2
  rcWorkArea.ntop = (rcParent.ntop + rcParent.nbottom)/2
  CALL enumdisplaymonitors(BYVAL 0, BYVAL 0, CODEPTR(MonitorEnumProc), BYVAL VARPTR(rcWorkArea))
                                                      'get rect of monitor window is shown


  pt.x = rcParent.nLeft + ((nParentWidth - nWidth) \ 2)                   'Calculate the new x coordinate and adjust for work area
  IF (pt.x < rcWorkArea.nLeft) THEN
     pt.x = rcWorkArea.nLeft
  ELSEIF ((pt.x + nWidth) > rcWorkArea.nRight) THEN
     pt.x = rcWorkArea.nRight - nWidth
  END IF

  pt.y = rcParent.nTop  + ((nParentHeight - nHeight) \ 2)                 'Calculate the new y corodinate and adjust for work area
  IF (pt.y < rcWorkArea.nTop) THEN
     pt.y = rcWorkArea.nTop
  ELSEIF ((pt.y + nHeight) > rcWorkArea.nBottom) THEN
     pt.y = rcWorkArea.nBottom - nHeight
  END IF

  IF (GetWindowLong(hwnd, %GWL_STYLE) AND %WS_CHILD) = %WS_CHILD THEN ScreenToClient hwndParent, pt
                                                      'Convert screen coordinates to client area coordinates

  SetWindowPos(hwnd, %NULL, pt.x, pt.y, 0, 0, %SWP_NOSIZE OR %SWP_NOZORDER)


END sub


'***********************************************************************************************


sub create_wait(hwnd as dword, hp as dword)
'***********************************************************************************************
' wait for hp to terminate
'***********************************************************************************************


  do 
    IF WaitForSingleObject(hp, 5) = 0 THEN
      exit loop
    END IF

'***********************************************************************************************
' maybe check for <esc> = user abort
'***********************************************************************************************
    sleep 50
  loop  


end sub


'***********************************************************************************************


FUNCTION jk_GetFileTime(d$) AS quad                   'filetime
'***********************************************************************************************
' get last write time
'***********************************************************************************************
LOCAL ft      AS Filetime
LOCAL hFile   AS DWORD


  hfile = CreateFile(d$ + chr$(0), %GENERIC_READ, 0, ByVal 0, %OPEN_EXISTING, %FILE_FLAG_SEQUENTIAL_SCAN Or %FILE_FLAG_NO_BUFFERING, ByVal 0 )
  IF hFile = %INVALID_HANDLE_VALUE THEN EXIT FUNCTION
  getfiletime hfile, byval 0, byval 0, ft
  CloseHandle(hfile)


  function = ft.qdatetime 
  
  
end function


'***********************************************************************************************


sub sendoutput(d$)
'***********************************************************************************************
' send data to output window
'***********************************************************************************************
LOCAL DataToSend AS COPYDATASTRUCT                    'Data to send structure


  DataToSend.lpData = STRPTR(d$)
  DataToSend.cbdata = LEN(d$) + 1
  DataToSend.dwData = %IDE_Output
  SendMessage ide_hwnd, %WM_COPYDATA, LEN(DataToSend), VARPTR(DataToSend)


end sub


'***********************************************************************************************


sub setfiletimes
'***********************************************************************************************
' retrieve and save last writetime for all open files
'***********************************************************************************************
local i as long
local n as long
local d$


  n = parsecount(f$, "|")
  redim ftw(1 to n)
  
  for i = 1 to n
    d$ = parse$(f$, "|", i)

    ftw(i) = jk_getfiletime(d$)
  next i


end sub


'***********************************************************************************************


sub reloadfiles
'***********************************************************************************************
' reload still existing files, close removed (without saving) in IDE
'***********************************************************************************************
local i as long
local n as long
LOCAL DataToSend AS COPYDATASTRUCT                    'Data to send structure
local d$


  n = parsecount(f$, "|")
  
  for i = n to 1 step -1                              'current file last -> remains active file
    d$ = parse$(f$, "|", i)
    if isfile(d$) then
      if ftw(i) <> jk_getfiletime(d$) then            'file has been changed
        DataToSend.lpData = STRPTR(d$)
        DataToSend.cbdata = LEN(d$) + 1
        DataToSend.dwData = %IDE_Open                 '(re)open in IDE
        SendMessage ide_hwnd, %WM_COPYDATA, LEN(DataToSend), VARPTR(DataToSend)
      end if
      
    else  
      DataToSend.lpData = STRPTR(d$)
      DataToSend.cbdata = LEN(d$) + 1
      DataToSend.dwData = %IDE_Close                  'close (without saving)
      SendMessage ide_hwnd, %WM_COPYDATA, LEN(DataToSend), VARPTR(DataToSend)
    end if
  next i


end sub


'***********************************************************************************************


function git_commit(byval flag as long) as long
'***********************************************************************************************
' user commit / auto commit at session end
'***********************************************************************************************
local n as long
local i as long
local c$
local d$
local d1$
local ztext as asciiz * 256


'***********************************************************************************************
' remove everything form index and add everything new (except for everything ignored [.gitignore])
' you may skip this, if you want to do this yourself
'***********************************************************************************************

  c$ = $dq + gitpath + $dq + " rm -f -r -q --cached . "
  if git_cmd(4, c$) = 0 then
    sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to remove !")

    function = 0
    exit function
  end if


  c$ = $dq + gitpath + $dq + " add --all"
'  c$ = $dq + gitpath + $dq + " add ." 'add existing don�t add new ones
  if git_cmd(4, c$) = 0 then
    sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to add !")

    function = 0
    exit function
  end if


'***********************************************************************************************
' remove special files from index as specified in exclude (#[...]
'***********************************************************************************************
  if isfile(cpath + ".git\info\exclude") then
    open cpath + ".git\info\exclude" for binary as #1
      get$ #1, lof(1), d$
    close #1  

    n = instr(ucase$(d$), "# -- DO NEVER COMMIT THESE FILES -- #")
    if n > 0 then
      d$ = mid$(d$, n)
      n = tally(d$, $crlf + "#[") + 1
      
      if n > 1 then
        for i = 2 to n
          d1$ = parse$(d$, $crlf + "#[", i)
          d1$ = extract$(d1$, "]")


          c$ = $dq + gitpath + $dq + " reset -- " + chr$(34) + d1$ + chr$(34)
'          c$ = $dq + gitpath + $dq + " reset -- " + chr$(34) + cpath + d1$ + chr$(34)
'odx(c$)
          if git_cmd(4, c$) = 0 then
            sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to reset !")

            function = 0
            exit function
          end if
        next i
      end if
    end if
  end if  


  if flag = 5 then exit function                      'exit here for commit with options


'***********************************************************************************************
' commit without options ...
' add version #  .ver
'***********************************************************************************************

    ztext = "V " + FORMAT$(HI(BYTE, (HI(WORD, ver)))) + "." + _
                   FORMAT$(LO(BYTE, (HI(WORD, ver)))) + "." + _
                   FORMAT$(HI(BYTE, (LO(WORD, ver)))) + "." + _
                   FORMAT$(LO(BYTE, (LO(WORD, ver)))) + " - "

  select case as long flag
    case 1
'      ztext = ztext + "(" + date$ + " " + time$ + "):  standard commit"
      ztext = "standard commit" + " - " + ztext + "(" + date$ + " " + time$ + ")"
    case 2
'      ztext = ztext + "(" + date$ + " " + time$ + "):  auto commit (session end)"
      ztext = "auto commit (session end)" + " - " + ztext + "(" + date$ + " " + time$ + ")"
  end select

odx(ztext)

  c$ = $dq + gitpath + $dq + " commit -a --allow-empty -m " + $dq + ztext + $dq
  sendoutput(CHR$(2)+HEX$(rgb(192,100,0),8)+c$)

  if git_cmd(3, c$) = 0 then
    sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to commit !")

    function = 0
    exit function
  end if


  postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


  function = 1


end function


'***********************************************************************************************


function git_open() as long
'***********************************************************************************************
' open a repository (checkout)
'***********************************************************************************************
local c$


  c$ = $dq + gitpath + $dq + " checkout -f"
  sendoutput(CHR$(2)+HEX$(rgb(192,100,0),8)+c$)

  if git_cmd(2, c$) = 0 then
    sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to checkout !")

    function = 0
    exit function
  end if


  postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


  function = 1
  

end function


'***********************************************************************************************


function git_new() as long
'***********************************************************************************************
' create a new repository
'***********************************************************************************************
local c$


  c$ = $dq + gitpath + $dq + " init"
  sendoutput(CHR$(2)+HEX$(rgb(192,100,0),8)+c$)

  if git_cmd(1, c$) = 0 then
    sendoutput(CHR$(1)+HEX$(%RED,8)+"Git Error: failed to create new repository ")

    function = 0
    exit function
  end if


  postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


  function = 1


end function


'***********************************************************************************************


function git_cmd(byval cmd as long, c$, optional byval flag as long) as long
'***********************************************************************************************
' execute git command + send output to output window
' flag = 0, don�t use input pipe
'***********************************************************************************************
LOCAL ExitCode     AS LONG                            'Exit code
LOCAL sa           AS SECURITY_ATTRIBUTES             'Security attributes
LOCAL hInReadPipe  AS DWORD                           'InRead pipe handle
LOCAL hInWritePipe AS DWORD                           'InWrite pipe handle
LOCAL hReadPipe    AS DWORD                           'Read pipe handle
LOCAL hWritePipe   AS DWORD                           'Write pipe handle
LOCAL SI           AS STARTUPINFO                     'Startup info structure
LOCAL pi           AS PROCESS_INFORMATION             'Process information structure
LOCAL BytesRead    AS DWORD                           'Bytes read
LOCAL BytesWritten AS DWORD                           'Bytes written
LOCAL chBuf        AS ASCIIZ * 1024                   'Character buffer
LOCAL chEOF        AS ASCIIZ * 12                     'End of file
LOCAL buffer       AS STRING                          'buffer
LOCAL szCmdLine    AS ASCIIZ * %MAX_PATH              'Command line
LOCAL Result       AS STRING                          'Result string


  sa.nLength = SIZEOF(SECURITY_ATTRIBUTES)
  sa.bInheritHandle = %TRUE

  IF CreatePipe(hReadPipe, hWritePipe, sa, BYVAL 0) = 0 THEN
    EXIT function
  end if


  IF flag then
    IF CreatePipe(hInReadPipe, hInWritePipe, sa, BYVAL 0) = 0 THEN
      EXIT function
    end if

    SI.cb          = SIZEOF(STARTUPINFO)
    SI.dwFlags     = %STARTF_USESHOWWINDOW OR %STARTF_USESTDHANDLES
    SI.wShowWindow = %SW_HIDE
    SI.hStdOutput  = hWritePipe
    SI.hStdError   = hWritePipe
    SI.hStdInput   = hInReadPipe

  else
    SI.cb          = SIZEOF(STARTUPINFO)
    SI.dwFlags     = %STARTF_USESHOWWINDOW OR %STARTF_USESTDHANDLES
    SI.wShowWindow = %SW_HIDE
    SI.hStdOutput  = hWritePipe
    SI.hStdError   = hWritePipe
  end if


  szCmdLine = c$
gol($crlf + "*** git_cmd - " + szcmdline + $crlf)


'local hfocus as dword
'  hfocus = getfocus



  IF CreateProcessA ("", szCmdLine, BYVAL 0&, BYVAL 0&, 1,  %NORMAL_PRIORITY_CLASS, _
       BYVAL 0&, "", si, pi) = 0 THEN EXIT function


''***********************************************************************************************
LOCAL msg AS tagMsg

  WHILE PeekMessage(msg, %NULL, %NULL, %NULL, %PM_REMOVE)
     TranslateMessage msg
     DispatchMessage msg
  WEND


  create_Wait(hwndm, pi.hprocess)
'***********************************************************************************************

  git_handle = pi.hprocess
  GetExitCodeProcess PI.hProcess, ExitCode
  IF ExitCode = %STILL_ACTIVE THEN
    TerminateProcess pi.hProcess, 0
    sendoutput(CHR$(1)+HEX$(%RED,8)+"git terminated due to timeout")
    git_handle = 0
  end if



  chEOF = "End Of Pipe"
  Result = ""

  DO
     IF WriteFile(hWritePipe, chEOF, LEN(chEOF), BytesWritten, BYVAL 0) = 0 THEN  EXIT DO
     IF BytesWritten <> LEN(chEOF) THEN  EXIT DO
     DO
        IF ReadFile(hReadPipe, chBuf, SIZEOF(chBuf), BytesRead, BYVAL 0) = 0 THEN EXIT DO
        IF BytesRead = 0 THEN EXIT DO
gol(LEFT$(chBuf, BytesRead))

        Result += LEFT$(chBuf, BytesRead)
'ods(result)
        IF RIGHT$(Result, LEN(chEOF)) = chEOF THEN
           Result = LEFT$(Result, LEN(Result) - LEN(chEOF))
           EXIT DO
        END IF
     LOOP
     EXIT DO
  LOOP

  CloseHandle hReadPipe
  CloseHandle hWritePipe
  if flag then
    CloseHandle hInReadPipe
    CloseHandle hInWritePipe
  end if
  CloseHandle pi.hThread
  CloseHandle pi.hProcess
  git_handle = 0


  buffer = result
  replace $lf with $crlf in buffer
  replace chEOF with "" in buffer


  IF LEN(buffer) THEN
    sendoutput(TRIM$(buffer, ANY CHR$(32, 0, 13, 10)))
    postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds
  END IF


  select case cmd
    case 1                                            'init
      if instr(ucase$(buffer), "INITIALIZED EMPTY GIT REPOSITORY") then
        function = 1
        
      else
        function = 0
      end if    

    case 2                                            'open
'      if instr(ucase$(buffer), "INITIALIZED EMPTY GIT REPOSITORY") then
        function = 1
        
'      else
'        function = 0
'      end if    

    case 3                                            'standard commit
      if instr(ucase$(buffer), "ERROR:") then
        function = 0
      else
        function = 1
      end if

    case 4                                            'remove/add
      if instr(ucase$(buffer), "ERROR:") then
        function = 0
      else
        function = 1
      end if

  end select


gol($crlf + "*** git_cmd - end" + $crlf)


end function


'***********************************************************************************************
'***********************************************************************************************


function pbmain() as long
'***********************************************************************************************
' main
'***********************************************************************************************
LOCAL uMsg        AS TAGMSG
LOCAL wce         AS WNDCLASSEX
#IF %DEF(%UNICODE)
LOCAL szClassName  AS WSTRINGZ  * 64
LOCAL szCaption    AS WSTRINGZ  * 64
#ELSE
LOCAL szClassName AS ASCIIZ * 64
LOCAL szCaption   AS ASCIIZ * 64
#ENDIF
LOCAL wstyle      AS DWORD
LOCAL xstyle      AS DWORD


  hinst = GetModuleHandle(byval 0)

'***********************************************************************************************
' adapt to your needs ...
'***********************************************************************************************
  gitpath = "E:\GIT\PortableGit\cmd\git.exe"
  guipath = "C:\Program Files\TortoiseGit\bin\TortoiseGitProc.exe"
'***********************************************************************************************

  cpath = curdir$ + "\"

  if command$(1) = "INIT" then                        'pass guipath via ini file
    AfxIniFileWrite(command$(2) + "jk-ide.ini", "Git", "GUI", guipath)
    exit function
  end if


  szClassName = "VCS_STUB"
  wce.cbSize = SIZEOF(wce)
  wce.STYLE = %CS_HREDRAW OR %CS_VREDRAW              'or %CS_DBLCLKS
  wce.lpfnWndProc = CODEPTR(wnd_proc)
  wce.hInstance = hInst
  wce.lpszClassName = VARPTR(szClassName)


  RegisterClassEx wce


  szCaption = ""
  wstyle = %WS_POPUP
  xstyle = %WS_EX_LEFT
  hwndm = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 0 ,0, 0, 0, 0, 0, hinst, byval 0)
  if hwndm = 0 then
ods("exit - failed to create VCS")
    exit function
  end if


  ShowWindow hwndm, %SW_Hide
  UpdateWindow hwndm                                   'hide main window


  DO WHILE GetMessage(uMsg, BYVAL %NULL, 0, 0)        'message loop
    IF IsDialogMessage(hwndb, uMsg) = 0 THEN          'dialog processing
      TranslateMessage uMsg
      DispatchMessage uMsg
    END IF
  LOOP


  FUNCTION = uMsg.wParam


end function


'***********************************************************************************************


function git_gui(hwnd as dword) as long
'***********************************************************************************************
' open "Git GUI"
'***********************************************************************************************


  Git_Create_Window(0)


end function


'***********************************************************************************************


function git_console() as long
'***********************************************************************************************
' open a git command console, preset git + repository path
'***********************************************************************************************
local i     as long
local n     as long
local x     as long
local pid   as dword
local hwnd  as dword
local ir()  as JK_input_Record
local jk_w  as dword
local t$


  t$ = "path %path%;" + pathname$(path, gitpath)      'set path for GIT
  pid = SHELL("cmd.exe")

  IMPORT ADDR "WriteConsoleInputA", "KERNEL32.DLL" to jk_w
  if jk_w = 0 then exit function


  do
    if attachconsole(pid) then
      sleep 50
      exit loop
    else
      n = n + 1
      if n > 50 then exit function                'prevent an endless loop, max. wait = 5 seconds

      sleep 10                                    'wait for console process to start
    end if
  loop


  n = len(t$)
  redim ir(n*2 + 2)                               '# of keystrokes * 2


  x = 0
  for i = 1 to n
    x = x + 1
    ir(x).EventType = %KEY_EVENT
    ir(x).bKeyDown         = 1
    ir(x).wRepeatCount     = 0
    ir(x).wVirtualKeyCode  = 0'asc(t$, i)
    ir(x).wVirtualScanCode = 0
    ir(x).AsciiChar        = mid$(t$, i, 1)
    ir(x).dwControlKeyState = 0

    x = x + 1
    ir(x).EventType = %KEY_EVENT
    ir(x).bKeyDown         = 0
    ir(x).wRepeatCount     = 0
    ir(x).wVirtualKeyCode  = 0'ASC(t$, i)
    ir(x).wVirtualScanCode = 0
    ir(x).AsciiChar        = mid$(t$, i, 1)
    ir(x).dwControlKeyState = 0
  next i


  x = x + 1
  ir(x).EventType = %KEY_EVENT
  ir(x).bKeyDown         = 1
  ir(x).wRepeatCount     = 0
  ir(x).wVirtualKeyCode  = %VK_Return
  ir(x).wVirtualScanCode = 0
  ir(x).AsciiChar        = $cr
  ir(x).dwControlKeyState = 0

  x = x + 1
  ir(x).EventType = %KEY_EVENT
  ir(x).bKeyDown         = 0
  ir(x).wRepeatCount     = 0
  ir(x).wVirtualKeyCode  = %VK_Return
  ir(x).wVirtualScanCode = 0
  ir(x).AsciiChar        = $cr
  ir(x).dwControlKeyState = 0


  call dword jk_w using JK_WriteConsoleInput(getstdhandle(%STD_INPUT_HANDLE), varptr(ir(1)), n*2 + 2, i)

  hwnd = getconsolewindow
  setforegroundwindow hwnd
  setwindowpos hwnd, %HWND_Top, 0, 0, 0, 0, %SWP_NoMove or %SWP_NoSize or %SWP_SHOWWINDOW


  freeconsole


  function = pid


end function


'***********************************************************************************************


FUNCTION wnd_proc(BYVAL hWnd AS dword, BYVAL wMsg AS dword, BYVAL wParam AS LONG, BYVAL lParam AS LONG) AS LONG
'***********************************************************************************************
' window procedure
'***********************************************************************************************
local i          as long
LOCAL pDataToGet AS COPYDATASTRUCT PTR                'Pointer to a CopyData structure
LOCAL p          AS ASCIIZ PTR                        'Pointer to buffer
local d$
local d1$
local c$
local ztext      as asciiz * %Max_Path


  SELECT CASE AS LONG wMsg
    CASE %WM_CREATE                                   'window creation
      postmessage hwnd, %WM_User + 1000, 0, 0         'post an initial message

      FUNCTION = 0                                    'success
      EXIT FUNCTION


    CASE %WM_Timer
      if wparam = 111 then
        killtimer hwnd, 111                           '10 seconds of initial inactivity -> exit
ods("exit - time limit reached")
        postmessage hwnd, %WM_Syscommand, %SC_Close, 0
        exit function

      elseif wparam = 112 then                        'wait for process (hp) to finish
        if waitforsingleobject(hp, 5) = 0 then
          killtimer hwnd, 112
          CloseHandle(hp)
          hp = 0
          if rflag = 0 then
            reloadfiles
          end if  
          postmessage hwnd, %WM_Syscommand, %SC_Close, 0
        end if
      end if


    CASE %WM_User + 1000                              'init message
      ide_hwnd = val("&H" + command$(1))              'JK-IDE handle
      cmd_id   = val("&H" + command$(2))              'command id
      pname    = command$(3)                          'project name
      ver      = val("&H" + command$(4))              'version number

      if iswindow(ide_hwnd) then
        postmessage ide_hwnd, %WM_App + 6000, 1, hwnd 'request for file list
        settimer hwnd, 111, 10000, BYVAL 0            'close after 10 seconds of initial inactivity

      else
ods("exit - no IDE handle")
        postmessage hwnd, %WM_Syscommand, %SC_Close, 0
        exit function
      end if


    CASE %WM_Copydata                                 'receive file list and do further processing
      pDataToGet = lParam
      p          = @pDataToGet.lpData

      SELECT CASE @pDataToGet.dwData
        CASE 1
          f$ = ucase$(@p)                             'get file list (first file = current file)
          if len(f$) then
            killtimer hwnd, 111                         'prevent premature exit
            setfiletimes
          end if

        case else                                     'otherwise exit
          postmessage hwnd, %WM_Syscommand, %SC_Close, 0
          exit function
      end select


      rflag = 0
'***********************************************************************************************
' process commands comming from the IDE, the IDE waits for VCS to exit
'***********************************************************************************************
      select case cmd_id
        case %VCS_Open
'odx(cpath)
'ods(curdir$)

          if isfolder(cpath + ".git") or isfile(cpath + ".git") then              'repo exists
            if isfile(cpath + "_git_") then
              sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "repository already open") 'show in IDE�s output window
              postmessage ide_hwnd, %WM_App + 6000, 2, 1                  'make toolbar button red
              postmessage ide_hwnd, %WM_App + 6000, 3, 0                  'close output window after 2.5 seconds

            else
              sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "open repository (checkout)") 'show in IDE�s output window
              if git_open then
                open cpath + "_git_" for binary as #1
                  put$ #1, "OPEN"
                  seteof(1)
                close #1  

                sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + " - OK")      'show in IDE�s output window
                postmessage ide_hwnd, %WM_App + 6000, 2, 1                'make toolbar button red
                postmessage ide_hwnd, %WM_App + 6000, 3, 0                'close output window after 2.5 seconds

                reloadfiles
              end if
            end if


          else                                        'create repo
            sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "create repository") 'show in IDE�s output window
            if git_new then
              open cpath + "_git_" for binary as #1
                put$ #1, "OPEN"
                seteof(1)
              close #1  

              open cpath + ".gitignore" for binary as #1
                put$ #1, "# list of files/filetypes to ignore in this worktree" + $crlf
                put$ #1, "Thumbs.db" + $crlf
                put$ #1, "Desktop.ini" + $crlf
                put$ #1, "_git_" + $crlf
                put$ #1, "*.tmp" + $crlf
                put$ #1, "*.tmc" + $crlf
                put$ #1, "*.tmi" + $crlf
                put$ #1, "*.log" + $crlf
                put$ #1, "*.bak" + $crlf
                put$ #1, "*.a" + $crlf
                put$ #1, "*.exe" + $crlf
                put$ #1, "*.dll" + $crlf
                put$ #1, "*.sll" + $crlf
              close #1  

              open cpath + ".gitattributes" for binary as #1
                put$ #1, "# Auto detect text files and perform LF normalization" + $crlf
                put$ #1, "* text=auto" + $crlf
                put$ #1, "# BASIC source files (need cr/lf line endings)" + $crlf
                put$ #1, "*.bas   eol=crlf" + $crlf
                put$ #1, "*.bi    eol=crlf" + $crlf
                put$ #1, "*.inc   eol=crlf" + $crlf
                put$ #1, "*.h     eol=crlf" + $crlf
                put$ #1, "*.bat   eol=crlf" + $crlf
                put$ #1, "*.txt   eol=crlf" + $crlf
                put$ #1, "*.jkp   eol=crlf" + $crlf
                put$ #1, "*.rc    eol=crlf" + $crlf
              close #1  


              sendoutput(CHR$(2)+HEX$(rgb(192,100,0),8) + " - OK") 'show in IDE�s output window
              postmessage ide_hwnd, %WM_App + 6000, 2, 1                  'make toolbar button red
              postmessage ide_hwnd, %WM_App + 6000, 3, 0                  'close output window after 2.5 seconds
            end if
          end if


        case %VCS_Changes
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "show changes")     'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          i = shell($dq + guipath + $dq + " /command:repostatus /path:" + cpath)
          hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
          settimer hwnd, 112, 100
          exit function
          

        case %VCS_Remote
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "open remote repository") 'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          if isfolder(cpath + ".git") then            'repo folder
            c$ = cpath + ".git\config"                'path to config file

          elseif isfile(cpath + ".git") then          'repo file
            open cpath + ".git" for binary as #1
              get$ #1, lof(1), d$
            close #1   

            d$ = ucase$(d$)
            d$ = remain$(d$, "GITDIR: ")
            d$ = extract$(d$, any $crlf)
            replace "/" with "\" in d$

            c$ = rtrim$(c$, "\") + "\config"          'path to config file
          end if

          if isfile(c$) then
            open c$ for binary as #1
              get$ #1, lof(1), d$
            close #1   

            i = instr(d$, "[remote ")                 'find remote
            i = instr(i, d$, $lf + $tab + "url = ")   'get url


            d1$ = extract$(i+2, d$, any $crlf)
            d1$ = remain$(d1$, " = ")                 'url as given in settings
            
'***********************************************************************************************
' process depending on url
'***********************************************************************************************

            if instr(d1$, "gitlab.com") then          'gitlab
              d1$ = "https://" + remain$(d1$, "@")
              replace "com:" with "com/" in d1$

              shellexecute(0, "open", byval strptr(d1$), "", "", %SW_SHOW)

'            elseif instr(d1$, "...") then             'other remotes

            end if

          else
            sendoutput(CHR$(3)+HEX$(%red,8) + "cannot find GIT config file ...") '
          end if



        case %VCS_Commit
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "commit")           'show in IDE�s output window

          git_commit(1)                               'standard commit
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


        case %VCS_Commit_Option
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "commit with options") 'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          git_commit(5)                               'prepare commit

          ztext = "V " + FORMAT$(HI(BYTE, (HI(WORD, ver)))) + "." + _
                         FORMAT$(LO(BYTE, (HI(WORD, ver)))) + "." + _
                         FORMAT$(HI(BYTE, (LO(WORD, ver)))) + "." + _
                         FORMAT$(LO(BYTE, (LO(WORD, ver)))) + " - "

          ztext = "standard commit" + " - " + ztext + "(" + date$ + " " + time$ + ")"
'          ztext = ztext + "(" + date$ + " " + time$ + "):  standard commit"

          i = shell($dq + guipath + $dq + " /command:commit /path:" + $dq + cpath + $dq + " /logmsg:" + $dq + ztext + $dq)
          hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
          rflag = 1                                   'don�t reload
          settimer hwnd, 112, 100
          exit function


        case %VCS_Auto_Commit
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "auto commit")      'show in IDE�s output window

          if isfile(cpath + "_git_") then             'if repo is open
            if git_commit(2) then                     'commit
              kill cpath + "_git_" 
              postmessage ide_hwnd, %WM_App + 6000, 2, 0                  'signal repo is closed
            end if
          end if

          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


        case %VCS_Close
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "close")            'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


          if isfile(cpath + "_git_") then             'if repo is open
            if git_commit(1) then                     'commit
              kill cpath + "_git_" 
              postmessage ide_hwnd, %WM_App + 6000, 2, 0                  'signal repo is closed
            end if
          end if

          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds


        case %VCS_Restore
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "restore (branch or previous commit)")           'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          if isfalse(isfile(cpath + "_git_")) then    'only if repo is closed
            if msgbox("This might overwrite existing files" + $crlf + "and delete files not present in the selected version", _
                         %MB_Iconexclamation or %MB_Topmost or %MB_OKCANCEL, " Warning") = %IDCancel then
              exit function
            end if

            i = shell($dq + guipath + $dq + " /command:switch /path:" + $dq + cpath + $dq)
            hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
            settimer hwnd, 112, 100
            exit function
          end if


        case %VCS_GUI
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "GUI")              'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          git_gui(hwnd)
          exit function
          

        case %VCS_Console
          sendoutput(CHR$(3)+HEX$(rgb(192,100,0),8) + "console")          'show in IDE�s output window
          postmessage ide_hwnd, %WM_App + 6000, 3, 0  'close output window after 2.5 seconds

          i = git_console                             'open git console
          hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
          settimer hwnd, 112, 100
          exit function

      end select

      postmessage hwnd, %WM_Syscommand, %SC_Close, 0  'exit


    CASE %WM_COMMAND


    CASE %WM_SYSCOMMAND
      SELECT CASE (wparam AND &HFFF0)
        CASE %SC_MINIMIZE

        CASE %SC_MAXIMIZE

        CASE %SC_RESTORE

        CASE %SC_KEYMENU

        CASE %SC_MOUSEMENU
      END SELECT


    CASE %WM_CLOSE


    CASE %WM_DESTROY                                  'exit - a good point for cleaning up
      if hp then
        closehandle hp
      end if  

'ods("main quit")
      PostQuitMessage 0                               'exit message loop in MAIN

      EXIT FUNCTION

  END SELECT


  FUNCTION = DefWindowProc(hWnd, wMsg, wParam, lParam)                    'default processing


END FUNCTION


'***********************************************************************************************
'***********************************************************************************************
'***********************************************************************************************
