

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility Constants_and_Definitions **************** (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%Git_Hwnd_Main  = 100                                 'ID for handle of main window
%Git_IDC_Btn_1  = 101                                 '[ Pull...]
%Git_IDC_Btn_2  = 102                                 '[ Fetch...]
%Git_IDC_Btn_3  = 103                                 '[ Push...]
%Git_IDC_Fr_4   = 104                                 '[ Global ]
%Git_IDC_Btn_5  = 105                                 '[ Diff]
%Git_IDC_Btn_6  = 106                                 '[ Diff (previous)]
%Git_IDC_Fr_7   = 107                                 '[ Diff ]
%Git_IDC_Fr_8   = 108                                 '[ Log ]
%Git_IDC_Fr_9   = 109                                 '[ Bisect ]
%Git_IDC_Fr_10  = 110                                 '[ Clean ]
%Git_IDC_Fr_11  = 111                                 '[ Repository ]
%Git_IDC_Fr_12  = 112                                 '[ Add ]
%Git_IDC_Fr_13  = 113                                 '[ Patch ]
%Git_IDC_Fr_14  = 114                                 '[ Common ]
%Git_IDC_Btn_15 = 115                                 '[ Show Log]
%Git_IDC_Btn_16 = 116                                 '[ Show Ref Log]
%Git_IDC_Btn_17 = 117                                 '[ References]
%Git_IDC_Btn_18 = 118                                 '[ Daemon]
%Git_IDC_Btn_19 = 119                                 '[ Revision Graph]
%Git_IDC_Btn_20 = 120                                 '[ Browse Repo]
%Git_IDC_Btn_21 = 121                                 '[ Modifications]
%Git_IDC_Btn_22 = 122                                 '[ Rebase...]
%Git_IDC_Btn_23 = 123                                 '[ Stash Changes]
%Git_IDC_Btn_24 = 124                                 '[ Bisect]
%Git_IDC_Btn_25 = 125                                 '[ Resolve...]
%Git_IDC_Btn_26 = 126                                 '[ Revert...]
%Git_IDC_Btn_27 = 127                                 '[ Clean up...]
%Git_IDC_Btn_28 = 128                                 '[ Switch/Checkout...]
%Git_IDC_Btn_29 = 129                                 '[ Merge...]
%Git_IDC_Btn_30 = 130                                 '[ Create Branch...]
%Git_IDC_Btn_31 = 131                                 '[ Create Tag...]
%Git_IDC_Btn_32 = 132                                 '[ Export...]
%Git_IDC_Btn_33 = 133                                 '[ Add...]
%Git_IDC_Btn_34 = 134                                 '[ Submodule Add...]
%Git_IDC_Btn_35 = 135                                 '[ Create Patch...]
%Git_IDC_Btn_36 = 136                                 '[ Apply Patch...]
%Git_IDC_Btn_37 = 137                                 '[ Settings]
%Git_IDC_Btn_38 = 138                                 '[ Help]
%Git_IDC_Btn_39 = 139                                 '[ About]
%Git_IDC_Btn_40 = 140                                 '[ Clone...]
%Git_IDC_Btn_41 = 141                                 '[ Sync...]
%Git_IDC_Btn_43 = 143                                 '[ Commit...]
%Git_IDC_Btn_44 = 144                                 '[ Stash Apply]
%Git_IDC_Btn_45 = 145                                 '[ Stash Pop]
%Git_IDC_Btn_46 = 146                                 '[ Stash List]
%Git_IDC_Btn_47 = 147                                 '[ Edit Conflicts]
%Git_IDC_Btn_48 = 148                                 '[ Rename...]
%Git_IDC_Btn_49 = 149                                 '[ Delete]
%Git_IDC_Btn_50 = 150                                 '[ Blame]
%Git_IDC_Btn_51 = 151                                 '[ Add to Ignore List]
%Git_IDC_Btn_52 = 152                                 '[ Submodule Update]
%Git_IDC_Btn_53 = 153                                 '[ Submodule Sync]
%Git_IDC_Btn_54 = 154                                 '[ Send Mail...]
%Git_IDC_Btn_55 = 155                                 '[ Import Ignore List]


DECLARE FUNCTION SetWindowTheme IMPORT "UxTheme.dll" ALIAS "SetWindowTheme" (BYVAL hwnd AS DWORD, _ 
                        BYREF pszSubAppName AS WSTRINGZ, BYREF pszSubIdList AS WSTRINGZ) AS LONG


'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility Begin_usercode_Definitions *************** (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code (Constants, Global/Threaded, Type/Union, #Include) here ...

'***********************************************************************************************
'***********************************************************************************************


FUNCTION Git_gethandle(BYVAL i AS DWORD, OPT BYVAL hWnd AS DWORD) AS DWORD
'***********************************************************************************************
' manage handles - do not edit this function!
'***********************************************************************************************
STATIC flag AS LONG
STATIC h() AS DWORD


  IF hwnd <> 0 THEN
    IF flag = 0 THEN
      REDIM h(100 TO 155)
      flag = 1
    ELSE
      IF i > UBOUND(h) THEN
        REDIM PRESERVE h(100 to i)
      END IF
    END IF

    IF i >= 100 THEN
      h(i) = hwnd
    END IF

  ELSE
    IF (i >= 100) and (i <= UBOUND(h)) THEN
      FUNCTION = h(i)
    ELSE
      FUNCTION = 0
    END IF
  END IF


END FUNCTION


'***********************************************************************************************


#IF %DEF(%UNICODE)
FUNCTION Git_getresources(BYVAL hwndc as DWORD, BYVAL l AS LONG, OPT BYVAL i AS DWORD, edat AS WSTRING) AS WSTRING
#ELSE
FUNCTION Git_getresources(BYVAL hwndc as DWORD, BYVAL l AS LONG, OPT BYVAL i AS DWORD, edat AS STRING) AS STRING
#ENDIF
'***********************************************************************************************
' return resource identifier(s) for list l ("|" separated), return only item i, if passed
'***********************************************************************************************
LOCAL idc AS DWORD
#IF %DEF(%UNICODE)
LOCAL dat AS WSTRING
#ELSE
LOCAL dat AS STRING
#ENDIF


  IF hwndc THEN
    idc = getdlgctrlid(hwndc)
  ELSE
    idc = %Git_Hwnd_Main
  END IF


  SELECT CASE idc
    CASE %Git_Hwnd_Main
      SELECT CASE l
        CASE 1
          dat = "000_GIT_32_ICO|GIT_16_ICO"

      END SELECT

    CASE ELSE
      dat = ""
  END SELECT


  IF i > 0 THEN
    dat = PARSE$(dat, "|", i)
    IF ISFALSE(ISMISSING(edat)) THEN
      edat = PARSE$(edat, $crlf, i)
    END IF
  END IF


  FUNCTION = dat


END FUNCTION


'***********************************************************************************************


#IF %DEF(%UNICODE)
FUNCTION Git_getimagelist(BYVAL hinst AS DWORD, BYVAL hwndc as DWORD, z AS WSTRING, OPT edat AS WSTRING) AS DWORD
#ELSE
FUNCTION Git_getimagelist(BYVAL hinst AS DWORD, BYVAL hwndc as DWORD, z AS ASCIIZ, OPT edat AS STRING) AS DWORD
#ENDIF
'***********************************************************************************************
' return imagelist for this window (hwndc), z = # of list or "E/D/H 16/24/32/48"
'***********************************************************************************************
LOCAL i        AS LONG
LOCAL n        AS LONG
LOCAL x        AS LONG
LOCAL idc      AS DWORD
LOCAL hdc      AS DWORD
LOCAL hmemdc   AS DWORD
LOCAL hbmp     AS DWORD
LOCAL hicon    AS DWORD
LOCAL himl     AS DWORD
LOCAL mc       AS DWORD
#IF %DEF(%UNICODE)
LOCAL ztext AS WSTRINGZ * %Max_Path
LOCAL dat AS WSTRING
#ELSE
LOCAL ztext AS ASCIIZ * %Max_Path
LOCAL dat AS STRING
#ENDIF


  z = UCASE$(REMOVE$(z, $spc))
  idc = getdlgctrlid(hwndc)


  IF (n > 0) and (i>0) THEN
    IF ISMISSING(edat) THEN
      dat = Git_getresources(hwndc, n)
    ELSE
      dat = Git_getresources(hwndc, n, 0, edat)
    END IF

    IF LEN(dat) THEN
      n = parsecount(dat, "|")

      himl = ImageList_Create(i, i, %ILC_MASK OR %ILC_COLOR32, n*2, n*3)

      IF himl THEN
        FOR x = 1 TO n
          ztext = PARSE$(dat, "|", x)
          REPLACE " " WITH "-" in ztext
          REPLACE "." WITH "_" in ztext

          SELECT CASE PARSE$(ztext, "_", -1)
            CASE "BMP"
              hbmp = LoadImage(hInst, ztext, %IMAGE_BITMAP, 0, 0, %LR_DEFAULTCOLOR)
              IF hbmp THEN
                hdc = GetDC(0)                        'handle to desktop DC
                hmemdc = CreateCompatibleDC(hdc)      'create compatible memory DC
                SelectObject(hmemdc,hbmp)             'put hbmp into memory DC
                ReleaseDC(0, hdc)                     'release desktop DC

                mc = GetPixel(hmemdc, 0, 0)           'get mask color
                deleteobject hmemdc                   'free resources

                ImageList_AddMasked himl, hbmp, mc
                DeleteObject hbmp
              END IF

            CASE "ICO", "CUR"
              hIcon = LoadIcon (hInst, ztext)
              IF hicon THEN
                ImageList_ReplaceIcon himl, -1, hIcon
                DestroyIcon hIcon
              END IF
          END SELECT
        NEXT x
      END IF
    END IF
  END IF


  FUNCTION = himl    


END FUNCTION


'***********************************************************************************************


FUNCTION centerwindow(BYVAL hwnd AS DWORD) AS LONG
'***********************************************************************************************
' center window (hwnd) in parent (if any) or in screen (main monitor only)
'***********************************************************************************************
LOCAL r  AS rect
LOCAL r1 AS rect
LOCAL pt AS point


  GetWindowRect hwnd, r                               'Get dialog position and size
  IF getparent(hwnd) = 0 THEN
    pt.x = getsystemmetrics(%SM_CXScreen)/2 - (r.nright - r.nleft)/2
    pt.y = getsystemmetrics(%SM_CyScreen)/2 - (r.nbottom - r.ntop)/2

  ELSE
    GetClientRect getparent(hwnd), r1                 'Get position and size of dialog parent (if any)
    pt.x = (r1.right - r1.left)/2 - (r.nright - r.nleft)/2
    pt.y = (r1.bottom - r1.top)/2 - (r.nbottom - r.ntop)/2
  END IF


  SetWindowPos hwnd, 0, pt.x, pt.y, 0, 0, %SWP_NOSIZE OR %SWP_NOZORDER


END FUNCTION


'***********************************************************************************************


FUNCTION Git_Create_Childs(BYVAL hinst AS DWORD, BYVAL hWnd AS DWORD, q AS SINGLE, ufont() AS DWORD, hmdi AS DWORD) AS DWORD
'***********************************************************************************************
' Create fonts, create and size childs, set font (this function gets overwritten with updates!)
'***********************************************************************************************
local i           AS LONG
local hdc         AS DWORD
local r           AS RECT
local lft         AS LOGFONT
LOCAL wce         AS WNDCLASSEX
#IF %DEF(%UNICODE)
LOCAL szClassName AS WSTRINGZ  * 64
LOCAL szCaption   AS WSTRINGZ  * 64
#ELSE
LOCAL szClassName AS ASCIIZ * 64
LOCAL szCaption   AS ASCIIZ * 64
#ENDIF
LOCAL hwndc       AS DWORD
LOCAL hwndtip     AS DWORD
LOCAL wstyle      AS DWORD
LOCAL xstyle      AS DWORD
LOCAL estyle      AS DWORD
LOCAL ti          AS TOOLINFO
LOCAL ccl         AS CLIENTCREATESTRUCT


  Git_gethandle(%Git_Hwnd_Main, hwnd)                 'save main handle

  hdc = getdc(0)
  i = GetDeviceCaps(hdc, %LOGPIXELSY)
  q = i/120                                           'dimensions according to dpi setting


DIM ufont(1 to 3)


  lft.lfHeight         = 9 * -1 * GetDeviceCaps(hDC, %LOGPIXELSY) / 72
  lft.lfWeight         = 400
  lft.lfItalic         = 0
  lft.lfEscapement     = 0
  lft.lfOrientation    = 0
  lft.lfUnderline      = 0
  lft.lfStrikeOut      = 0
  lft.lfCharset        =  0
  lft.lfPitchAndFamily =  34
  lft.lfFaceName       = "Tahoma"
  ufont(1) = CreateFontIndirect(lft)

  lft.lfHeight         = 17 * -1 * GetDeviceCaps(hDC, %LOGPIXELSY) / 72
  lft.lfWeight         = 700
  lft.lfItalic         = 0
  lft.lfEscapement     = 0
  lft.lfOrientation    = 0
  lft.lfUnderline      = 0
  lft.lfStrikeOut      = 0
  lft.lfCharset        =  0
  lft.lfPitchAndFamily =  0
  lft.lfFaceName       = "Tahoma"
  ufont(2) = CreateFontIndirect(lft)

  lft.lfHeight         = 28 * -1 * GetDeviceCaps(hDC, %LOGPIXELSY) / 72
  lft.lfWeight         = 400
  lft.lfItalic         = 0
  lft.lfEscapement     = 0
  lft.lfOrientation    = 0
  lft.lfUnderline      = 0
  lft.lfStrikeOut      = 0
  lft.lfCharset        =  0
  lft.lfPitchAndFamily =  0
  lft.lfFaceName       = "Arial"
  ufont(3) = CreateFontIndirect(lft)

  releasedc(0, hdc)


'***********************************************************************************************
' Create child window(s) according to tab order
'***********************************************************************************************


#utility Control #40  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Clone...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 30 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_40, hinst, BYVAL 0) '[ Clone...]
  Git_gethandle(%Git_IDC_Btn_40, hwndc)               '[ Clone...]
  sendmessage Git_gethandle(%Git_IDC_Btn_40), %WM_SETFONT, ufont(1), 0    '[ Clone...]


#utility Control #1  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Pull...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 60 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_1, hinst, BYVAL 0) '[ Pull...]
  Git_gethandle(%Git_IDC_Btn_1, hwndc)                '[ Pull...]
  sendmessage Git_gethandle(%Git_IDC_Btn_1), %WM_SETFONT, ufont(1), 0     '[ Pull...]


#utility Control #2  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Fetch...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 90 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_2, hinst, BYVAL 0) '[ Fetch...]
  Git_gethandle(%Git_IDC_Btn_2, hwndc)                '[ Fetch...]
  sendmessage Git_gethandle(%Git_IDC_Btn_2), %WM_SETFONT, ufont(1), 0     '[ Fetch...]


#utility Control #3  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Push...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 120 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_3, hinst, BYVAL 0) '[ Push...]
  Git_gethandle(%Git_IDC_Btn_3, hwndc)                '[ Push...]
  sendmessage Git_gethandle(%Git_IDC_Btn_3), %WM_SETFONT, ufont(1), 0     '[ Push...]


#utility Control #41  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Sync...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 150 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_41, hinst, BYVAL 0) '[ Sync...]
  Git_gethandle(%Git_IDC_Btn_41, hwndc)               '[ Sync...]
  sendmessage Git_gethandle(%Git_IDC_Btn_41), %WM_SETFONT, ufont(1), 0    '[ Sync...]


#utility Control #43  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Commit...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 210 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_43, hinst, BYVAL 0) '[ Commit...]
  Git_gethandle(%Git_IDC_Btn_43, hwndc)               '[ Commit...]
  sendmessage Git_gethandle(%Git_IDC_Btn_43), %WM_SETFONT, ufont(1), 0    '[ Commit...]


#utility Control #28  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Switch/Checkout...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 240 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_28, hinst, BYVAL 0) '[ Switch/Checkout...]
  Git_gethandle(%Git_IDC_Btn_28, hwndc)               '[ Switch/Checkout...]
  sendmessage Git_gethandle(%Git_IDC_Btn_28), %WM_SETFONT, ufont(1), 0    '[ Switch/Checkout...]


#utility Control #29  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Merge...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 270 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_29, hinst, BYVAL 0) '[ Merge...]
  Git_gethandle(%Git_IDC_Btn_29, hwndc)               '[ Merge...]
  sendmessage Git_gethandle(%Git_IDC_Btn_29), %WM_SETFONT, ufont(1), 0    '[ Merge...]


#utility Control #30  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Create Branch...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 300 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_30, hinst, BYVAL 0) '[ Create Branch...]
  Git_gethandle(%Git_IDC_Btn_30, hwndc)               '[ Create Branch...]
  sendmessage Git_gethandle(%Git_IDC_Btn_30), %WM_SETFONT, ufont(1), 0    '[ Create Branch...]


#utility Control #31  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Create Tag...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 330 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_31, hinst, BYVAL 0) '[ Create Tag...]
  Git_gethandle(%Git_IDC_Btn_31, hwndc)               '[ Create Tag...]
  sendmessage Git_gethandle(%Git_IDC_Btn_31), %WM_SETFONT, ufont(1), 0    '[ Create Tag...]


#utility Control #32  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Export...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 360 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_32, hinst, BYVAL 0) '[ Export...]
  Git_gethandle(%Git_IDC_Btn_32, hwndc)               '[ Export...]
  sendmessage Git_gethandle(%Git_IDC_Btn_32), %WM_SETFONT, ufont(1), 0    '[ Export...]


#utility Control #5  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Diff")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 420 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_5, hinst, BYVAL 0) '[ Diff]
  Git_gethandle(%Git_IDC_Btn_5, hwndc)                '[ Diff]
  sendmessage Git_gethandle(%Git_IDC_Btn_5), %WM_SETFONT, ufont(1), 0     '[ Diff]


#utility Control #6  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Diff (previous)")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 450 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_6, hinst, BYVAL 0) '[ Diff (previous)]
  Git_gethandle(%Git_IDC_Btn_6, hwndc)                '[ Diff (previous)]
  sendmessage Git_gethandle(%Git_IDC_Btn_6), %WM_SETFONT, ufont(1), 0     '[ Diff (previous)]


#utility Control #24  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Bisect")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 20 * q, 510 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_24, hinst, BYVAL 0) '[ Bisect]
  Git_gethandle(%Git_IDC_Btn_24, hwndc)               '[ Bisect]
  sendmessage Git_gethandle(%Git_IDC_Btn_24), %WM_SETFONT, ufont(1), 0    '[ Bisect]


#utility Control #47  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Edit Conflicts")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 30 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_47, hinst, BYVAL 0) '[ Edit Conflicts]
  Git_gethandle(%Git_IDC_Btn_47, hwndc)               '[ Edit Conflicts]
  sendmessage Git_gethandle(%Git_IDC_Btn_47), %WM_SETFONT, ufont(1), 0    '[ Edit Conflicts]


#utility Control #25  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Resolve...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 60 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_25, hinst, BYVAL 0) '[ Resolve...]
  Git_gethandle(%Git_IDC_Btn_25, hwndc)               '[ Resolve...]
  sendmessage Git_gethandle(%Git_IDC_Btn_25), %WM_SETFONT, ufont(1), 0    '[ Resolve...]


#utility Control #48  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Rename...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 90 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_48, hinst, BYVAL 0) '[ Rename...]
  Git_gethandle(%Git_IDC_Btn_48, hwndc)               '[ Rename...]
  sendmessage Git_gethandle(%Git_IDC_Btn_48), %WM_SETFONT, ufont(1), 0    '[ Rename...]


#utility Control #49  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Delete (keep local)")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 120 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_49, hinst, BYVAL 0) '[ Delete]
  Git_gethandle(%Git_IDC_Btn_49, hwndc)               '[ Delete]
  sendmessage Git_gethandle(%Git_IDC_Btn_49), %WM_SETFONT, ufont(1), 0    '[ Delete]


#utility Control #26  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Revert...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 150 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_26, hinst, BYVAL 0) '[ Revert...]
  Git_gethandle(%Git_IDC_Btn_26, hwndc)               '[ Revert...]
  sendmessage Git_gethandle(%Git_IDC_Btn_26), %WM_SETFONT, ufont(1), 0    '[ Revert...]


#utility Control #27  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Clean up...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 180 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_27, hinst, BYVAL 0) '[ Clean up...]
  Git_gethandle(%Git_IDC_Btn_27, hwndc)               '[ Clean up...]
  sendmessage Git_gethandle(%Git_IDC_Btn_27), %WM_SETFONT, ufont(1), 0    '[ Clean up...]


#utility Control #33  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Add...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 240 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_33, hinst, BYVAL 0) '[ Add...]
  Git_gethandle(%Git_IDC_Btn_33, hwndc)               '[ Add...]
  sendmessage Git_gethandle(%Git_IDC_Btn_33), %WM_SETFONT, ufont(1), 0    '[ Add...]


#utility Control #50  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Blame")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 270 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_50, hinst, BYVAL 0) '[ Blame]
  Git_gethandle(%Git_IDC_Btn_50, hwndc)               '[ Blame]
  sendmessage Git_gethandle(%Git_IDC_Btn_50), %WM_SETFONT, ufont(1), 0    '[ Blame]


#utility Control #51  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Edit Ignore List ...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 300 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_51, hinst, BYVAL 0) '[ Add to Ignore List]
  Git_gethandle(%Git_IDC_Btn_51, hwndc)               '[ Add to Ignore List]
  sendmessage Git_gethandle(%Git_IDC_Btn_51), %WM_SETFONT, ufont(1), 0    '[ Add to Ignore List]


#utility Control #34  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Submodule Add...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 330 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_34, hinst, BYVAL 0) '[ Submodule Add...]
  Git_gethandle(%Git_IDC_Btn_34, hwndc)               '[ Submodule Add...]
  sendmessage Git_gethandle(%Git_IDC_Btn_34), %WM_SETFONT, ufont(1), 0    '[ Submodule Add...]


#utility Control #52  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Submodule Update")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 360 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_52, hinst, BYVAL 0) '[ Submodule Update]
  Git_gethandle(%Git_IDC_Btn_52, hwndc)               '[ Submodule Update]
  sendmessage Git_gethandle(%Git_IDC_Btn_52), %WM_SETFONT, ufont(1), 0    '[ Submodule Update]


#utility Control #53  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Submodule Sync")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 390 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_53, hinst, BYVAL 0) '[ Submodule Sync]
  Git_gethandle(%Git_IDC_Btn_53, hwndc)               '[ Submodule Sync]
  sendmessage Git_gethandle(%Git_IDC_Btn_53), %WM_SETFONT, ufont(1), 0    '[ Submodule Sync]


#utility Control #55  (Do not edit this line !!!)
  szClassName = "Button"
'  szCaption = UCS(" Edit Ignore List...")
  szCaption = UCS(" Edit Exclude List...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 420 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_55, hinst, BYVAL 0) '[ Import Ignore List]
  Git_gethandle(%Git_IDC_Btn_55, hwndc)               '[ Import Ignore List]
  sendmessage Git_gethandle(%Git_IDC_Btn_55), %WM_SETFONT, ufont(1), 0    '[ Import Ignore List]


#utility Control #35  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Create Patch...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 480 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_35, hinst, BYVAL 0) '[ Create Patch...]
  Git_gethandle(%Git_IDC_Btn_35, hwndc)               '[ Create Patch...]
  sendmessage Git_gethandle(%Git_IDC_Btn_35), %WM_SETFONT, ufont(1), 0    '[ Create Patch...]


#utility Control #36  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Apply Patch...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 200 * q, 510 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_36, hinst, BYVAL 0) '[ Apply Patch...]
  Git_gethandle(%Git_IDC_Btn_36, hwndc)               '[ Apply Patch...]
  sendmessage Git_gethandle(%Git_IDC_Btn_36), %WM_SETFONT, ufont(1), 0    '[ Apply Patch...]


#utility Control #15  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Show Log")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 30 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_15, hinst, BYVAL 0) '[ Show Log]
  Git_gethandle(%Git_IDC_Btn_15, hwndc)               '[ Show Log]
  sendmessage Git_gethandle(%Git_IDC_Btn_15), %WM_SETFONT, ufont(1), 0    '[ Show Log]


#utility Control #16  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Show Ref Log")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 60 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_16, hinst, BYVAL 0) '[ Show Ref Log]
  Git_gethandle(%Git_IDC_Btn_16, hwndc)               '[ Show Ref Log]
  sendmessage Git_gethandle(%Git_IDC_Btn_16), %WM_SETFONT, ufont(1), 0    '[ Show Ref Log]


#utility Control #17  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" References")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 90 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_17, hinst, BYVAL 0) '[ References]
  Git_gethandle(%Git_IDC_Btn_17, hwndc)               '[ References]
  sendmessage Git_gethandle(%Git_IDC_Btn_17), %WM_SETFONT, ufont(1), 0    '[ References]


#utility Control #18  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Daemon")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 120 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_18, hinst, BYVAL 0) '[ Daemon]
  Git_gethandle(%Git_IDC_Btn_18, hwndc)               '[ Daemon]
  sendmessage Git_gethandle(%Git_IDC_Btn_18), %WM_SETFONT, ufont(1), 0    '[ Daemon]


#utility Control #19  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Revision Graph")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 150 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_19, hinst, BYVAL 0) '[ Revision Graph]
  Git_gethandle(%Git_IDC_Btn_19, hwndc)               '[ Revision Graph]
  sendmessage Git_gethandle(%Git_IDC_Btn_19), %WM_SETFONT, ufont(1), 0    '[ Revision Graph]


#utility Control #20  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Browse Repo")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 180 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_20, hinst, BYVAL 0) '[ Browse Repo]
  Git_gethandle(%Git_IDC_Btn_20, hwndc)               '[ Browse Repo]
  sendmessage Git_gethandle(%Git_IDC_Btn_20), %WM_SETFONT, ufont(1), 0    '[ Browse Repo]


#utility Control #21  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Modifications")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 210 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_21, hinst, BYVAL 0) '[ Modifications]
  Git_gethandle(%Git_IDC_Btn_21, hwndc)               '[ Modifications]
  sendmessage Git_gethandle(%Git_IDC_Btn_21), %WM_SETFONT, ufont(1), 0    '[ Modifications]


#utility Control #22  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Rebase...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 240 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_22, hinst, BYVAL 0) '[ Rebase...]
  Git_gethandle(%Git_IDC_Btn_22, hwndc)               '[ Rebase...]
  sendmessage Git_gethandle(%Git_IDC_Btn_22), %WM_SETFONT, ufont(1), 0    '[ Rebase...]


#utility Control #23  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Stash Changes")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 270 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_23, hinst, BYVAL 0) '[ Stash Changes]
  Git_gethandle(%Git_IDC_Btn_23, hwndc)               '[ Stash Changes]
  sendmessage Git_gethandle(%Git_IDC_Btn_23), %WM_SETFONT, ufont(1), 0    '[ Stash Changes]


#utility Control #44  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Stash Apply")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 300 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_44, hinst, BYVAL 0) '[ Stash Apply]
  Git_gethandle(%Git_IDC_Btn_44, hwndc)               '[ Stash Apply]
  sendmessage Git_gethandle(%Git_IDC_Btn_44), %WM_SETFONT, ufont(1), 0    '[ Stash Apply]


#utility Control #45  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Stash Pop")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 330 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_45, hinst, BYVAL 0) '[ Stash Pop]
  Git_gethandle(%Git_IDC_Btn_45, hwndc)               '[ Stash Pop]
  sendmessage Git_gethandle(%Git_IDC_Btn_45), %WM_SETFONT, ufont(1), 0    '[ Stash Pop]


#utility Control #46  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Stash List")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 360 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_46, hinst, BYVAL 0) '[ Stash List]
  Git_gethandle(%Git_IDC_Btn_46, hwndc)               '[ Stash List]
  sendmessage Git_gethandle(%Git_IDC_Btn_46), %WM_SETFONT, ufont(1), 0    '[ Stash List]


#utility Control #54  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Send Mail...")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 420 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_54, hinst, BYVAL 0) '[ Send Mail...]
  Git_gethandle(%Git_IDC_Btn_54, hwndc)               '[ Send Mail...]
  sendmessage Git_gethandle(%Git_IDC_Btn_54), %WM_SETFONT, ufont(1), 0    '[ Send Mail...]


#utility Control #37  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Settings")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 450 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_37, hinst, BYVAL 0) '[ Settings]
  Git_gethandle(%Git_IDC_Btn_37, hwndc)               '[ Settings]
  sendmessage Git_gethandle(%Git_IDC_Btn_37), %WM_SETFONT, ufont(1), 0    '[ Settings]


#utility Control #38  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" Help")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 480 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_38, hinst, BYVAL 0) '[ Help]
  Git_gethandle(%Git_IDC_Btn_38, hwndc)               '[ Help]
  sendmessage Git_gethandle(%Git_IDC_Btn_38), %WM_SETFONT, ufont(1), 0    '[ Help]


#utility Control #39  (Do not edit this line !!!)
  szClassName = "Button"
  szCaption = UCS(" About")
  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %BS_PUSHBUTTON OR %BS_LEFT OR %BS_VCENTER
  xstyle = %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 380 * q, 510 * q, 160 * q, 30 * q, hwnd, %Git_IDC_Btn_39, hinst, BYVAL 0) '[ About]
  Git_gethandle(%Git_IDC_Btn_39, hwndc)               '[ About]
  sendmessage Git_gethandle(%Git_IDC_Btn_39), %WM_SETFONT, ufont(1), 0    '[ About]


#utility Control #4  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Global ")
'  wstyle = %WS_TABSTOP OR %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 10 * q, 10 * q, 180 * q, 190 * q, hwnd, %Git_IDC_Fr_4, hinst, BYVAL 0) '[ Global ]
  Git_gethandle(%Git_IDC_Fr_4, hwndc)                 '[ Global ]
  sendmessage Git_gethandle(%Git_IDC_Fr_4), %WM_SETFONT, ufont(1), 0      '[ Global ]


#utility Control #11  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Repository ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 10 * q, 190 * q, 180 * q, 220 * q, hwnd, %Git_IDC_Fr_11, hinst, BYVAL 0) '[ Repository ]
  Git_gethandle(%Git_IDC_Fr_11, hwndc)                '[ Repository ]
  sendmessage Git_gethandle(%Git_IDC_Fr_11), %WM_SETFONT, ufont(1), 0     '[ Repository ]


#utility Control #7  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Diff ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 10 * q, 400 * q, 180 * q, 100 * q, hwnd, %Git_IDC_Fr_7, hinst, BYVAL 0) '[ Diff ]
  Git_gethandle(%Git_IDC_Fr_7, hwndc)                 '[ Diff ]
  sendmessage Git_gethandle(%Git_IDC_Fr_7), %WM_SETFONT, ufont(1), 0      '[ Diff ]


#utility Control #9  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Bisect ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 10 * q, 490 * q, 180 * q, 60 * q, hwnd, %Git_IDC_Fr_9, hinst, BYVAL 0) '[ Bisect ]
  Git_gethandle(%Git_IDC_Fr_9, hwndc)                 '[ Bisect ]
  sendmessage Git_gethandle(%Git_IDC_Fr_9), %WM_SETFONT, ufont(1), 0      '[ Bisect ]


#utility Control #10  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Clean ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 190 * q, 10 * q, 180 * q, 220 * q, hwnd, %Git_IDC_Fr_10, hinst, BYVAL 0) '[ Clean ]
  Git_gethandle(%Git_IDC_Fr_10, hwndc)                '[ Clean ]
  sendmessage Git_gethandle(%Git_IDC_Fr_10), %WM_SETFONT, ufont(1), 0     '[ Clean ]


#utility Control #12  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Add ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 190 * q, 220 * q, 180 * q, 250 * q, hwnd, %Git_IDC_Fr_12, hinst, BYVAL 0) '[ Add ]
  Git_gethandle(%Git_IDC_Fr_12, hwndc)                '[ Add ]
  sendmessage Git_gethandle(%Git_IDC_Fr_12), %WM_SETFONT, ufont(1), 0     '[ Add ]


#utility Control #13  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Patch ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 190 * q, 460 * q, 180 * q, 90 * q, hwnd, %Git_IDC_Fr_13, hinst, BYVAL 0) '[ Patch ]
  Git_gethandle(%Git_IDC_Fr_13, hwndc)                '[ Patch ]
  sendmessage Git_gethandle(%Git_IDC_Fr_13), %WM_SETFONT, ufont(1), 0     '[ Patch ]


#utility Control #14  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Common ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 370 * q, 400 * q, 180 * q, 150 * q, hwnd, %Git_IDC_Fr_14, hinst, BYVAL 0) '[ Common ]
  Git_gethandle(%Git_IDC_Fr_14, hwndc)                '[ Common ]
  sendmessage Git_gethandle(%Git_IDC_Fr_14), %WM_SETFONT, ufont(1), 0     '[ Common ]


#utility Control #8  (Do not edit this line !!!)
  szClassName = "BUTTON"
  szCaption = UCS(" Log ")
  wstyle = %WS_CHILD OR %WS_VISIBLE OR %WS_CLIPSIBLINGS OR %WS_GROUP OR %BS_CENTER OR %BS_GROUPBOX
  xstyle = %WS_EX_TRANSPARENT OR %WS_EX_LEFT OR %WS_EX_LTRREADING
  hwndc = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 370 * q, 10 * q, 180 * q, 400 * q, hwnd, %Git_IDC_Fr_8, hinst, BYVAL 0) '[ Log ]
  Git_gethandle(%Git_IDC_Fr_8, hwndc)                 '[ Log ]
  sendmessage Git_gethandle(%Git_IDC_Fr_8), %WM_SETFONT, ufont(1), 0      '[ Log ]


'***********************************************************************************************
' Set size + position of main window
'***********************************************************************************************


  r.left   = 340 * q
  r.right  = r.left + 560 * q
  r.top    = 130 * q
  r.bottom = r.top + 560 * q

  adjustwindowrectex r, getwindowlong(hwnd, %GWL_Style), 0, getwindowlong(hwnd, %GWL_EXStyle)
  setwindowpos hwnd, 0, 340 * q, 130 * q, (r.right - r.left), (r.bottom - r.top), %SWP_NoZorder


END FUNCTION


'***********************************************************************************************


sub Git_seticons(hinst as dword)
'***********************************************************************************************
'
'***********************************************************************************************
local hicon as dword

  hicon = loadimage(hinst, "CLONE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_40), %BM_Setimage, %IMAGE_ICON, hicon '[ Clone...]
  destroyicon hicon

  hicon = loadimage(hinst, "PULL", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_1), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "FETCH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_2), %BM_Setimage, %IMAGE_ICON, hicon '[ Fetch...]
  destroyicon hicon

  hicon = loadimage(hinst, "PUSH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_3), %BM_Setimage, %IMAGE_ICON, hicon '[ Push...]
  destroyicon hicon

  hicon = loadimage(hinst, "SYNC", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_41), %BM_Setimage, %IMAGE_ICON, hicon '[ Sync...]
  destroyicon hicon


  hicon = loadimage(hinst, "COMMIT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_43), %BM_Setimage, %IMAGE_ICON, hicon '[ Commit...]
  destroyicon hicon

  hicon = loadimage(hinst, "CHECKOUT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_28), %BM_Setimage, %IMAGE_ICON, hicon '[ Switch/Checkout...]
  destroyicon hicon

  hicon = loadimage(hinst, "MERGE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_29), %BM_Setimage, %IMAGE_ICON, hicon '[ Merge...]
  destroyicon hicon

  hicon = loadimage(hinst, "BRANCH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_30), %BM_Setimage, %IMAGE_ICON, hicon '[ Create Branch...]
  destroyicon hicon

  hicon = loadimage(hinst, "TAG", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_31), %BM_Setimage, %IMAGE_ICON, hicon '[ Create Tag...]
  destroyicon hicon

  hicon = loadimage(hinst, "EXPORT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_32), %BM_Setimage, %IMAGE_ICON, hicon '[ Export...]
  destroyicon hicon


  hicon = loadimage(hinst, "DIFF", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_5), %BM_Setimage, %IMAGE_ICON, hicon '[ Diff]
  sendmessage Git_gethandle(%Git_IDC_Btn_6), %BM_Setimage, %IMAGE_ICON, hicon '[ Diff (previous)]
  destroyicon hicon


  hicon = loadimage(hinst, "BISECT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_24), %BM_Setimage, %IMAGE_ICON, hicon '[ Bisect]
  destroyicon hicon


  hicon = loadimage(hinst, "CONFLICTS", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_47), %BM_Setimage, %IMAGE_ICON, hicon '[ Edit Conflicts]
  destroyicon hicon

  hicon = loadimage(hinst, "RESOLVE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_25), %BM_Setimage, %IMAGE_ICON, hicon '[ Resolve...]
  destroyicon hicon

  hicon = loadimage(hinst, "RENAME", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_48), %BM_Setimage, %IMAGE_ICON, hicon '[ Rename...]
  destroyicon hicon

  hicon = loadimage(hinst, "DELETE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_49), %BM_Setimage, %IMAGE_ICON, hicon '[ Delete]
  destroyicon hicon

  hicon = loadimage(hinst, "REVERT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_26), %BM_Setimage, %IMAGE_ICON, hicon '[ Revert...]
  destroyicon hicon

  hicon = loadimage(hinst, "CLEAN", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_27), %BM_Setimage, %IMAGE_ICON, hicon '[ Clean up...]
  destroyicon hicon

  hicon = loadimage(hinst, "ADDTO", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_33), %BM_Setimage, %IMAGE_ICON, hicon '[ Add...]
  destroyicon hicon

  hicon = loadimage(hinst, "BLAME", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_50), %BM_Setimage, %IMAGE_ICON, hicon '[ Blame]
  destroyicon hicon

  hicon = loadimage(hinst, "IMPORT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_51), %BM_Setimage, %IMAGE_ICON, hicon '[ Add to Ignore List]
  destroyicon hicon

  hicon = loadimage(hinst, "ADDTO", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_34), %BM_Setimage, %IMAGE_ICON, hicon '[ Submodule Add...]
  destroyicon hicon

  hicon = loadimage(hinst, "SUBUPDATE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_52), %BM_Setimage, %IMAGE_ICON, hicon '[ Submodule Update]
  destroyicon hicon

  hicon = loadimage(hinst, "SUBSYNC", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_53), %BM_Setimage, %IMAGE_ICON, hicon '[ Submodule Sync]
  destroyicon hicon

  hicon = loadimage(hinst, "IMPORT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_55), %BM_Setimage, %IMAGE_ICON, hicon '[ Import Ignore List]
  destroyicon hicon


  hicon = loadimage(hinst, "CREATEPATCH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_35), %BM_Setimage, %IMAGE_ICON, hicon '[ Create Patch...]
  destroyicon hicon

  hicon = loadimage(hinst, "APPLYPATCH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_36), %BM_Setimage, %IMAGE_ICON, hicon '[ Apply Patch...]
  destroyicon hicon


  hicon = loadimage(hinst, "SHOWLOG", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_15), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "SHOWLOG", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_16), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "BROWSE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_17), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "DAEMON", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_18), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "GRAPH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_19), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "BROWSE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_20), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "CHECK", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_21), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "REBASE", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_22), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "STASH", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_23), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "STASHPOP", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_44), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "STASHPOP", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_45), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "SHOWLOG", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_46), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon


  hicon = loadimage(hinst, "MAIL", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_54), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "SETTINGS", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_37), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "SHOWHELP", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_38), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon

  hicon = loadimage(hinst, "ABOUT", %IMAGE_icon, 16, 16, %LR_DEFAULTSIZE)
  sendmessage Git_gethandle(%Git_IDC_Btn_39), %BM_Setimage, %IMAGE_ICON, hicon '[ Pull...]
  destroyicon hicon


end sub


'***********************************************************************************************

FUNCTION Git_WindowProc(BYVAL hWnd AS DWORD, BYVAL wMsg AS DWORD, BYVAL wParam AS DWORD, BYVAL lParam AS LONG) AS LONG
'***********************************************************************************************
' Main window procedure (this function gets overwritten with updates!
'***********************************************************************************************                               
LOCAL i           AS LONG                               
LOCAL pnmh        AS NMHDR PTR                               
LOCAL pminmax     AS MINMAXINFO ptr
LOCAL pNMCUST     AS NMCUSTOMDRAW PTR
#IF %DEF(%UNICODE)
LOCAL ztext       AS WSTRINGZ  * %MAX_PATH
#ELSE
LOCAL ztext       AS ASCIIZ * %MAX_PATH
#ENDIF
local d$                              
                               
STATIC hinst      AS DWORD                               
STATIC q          AS SINGLE                           'window stretch factor
STATIC ufont()    AS DWORD                            'user defined fonts
STATIC hmdi       AS DWORD                            'mdi client window handle
STATIC hwndtip    AS DWORD 
static xflag      as long


  SELECT CASE AS LONG wMsg
    CASE %WM_CREATE                                   'creation of the main window
      hinst = getmodulehandle(BYVAL 0)

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_WM_Create_start +++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_WM_Create_start +++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      hwndtip = Git_Create_Childs(hinst, hwnd, q, ufont(), hmdi)          'create child window(s)


'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_WM_Create_end +++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...


      Git_seticons(hinst)                             'add icons to buttons

      if isfile(cpath + "_git_") then
'        enablewindow git_gethandle(%Git_IDC_Btn_28), 0
      end if

      setfocus git_gethandle(%Git_IDC_Btn_40)

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_WM_Create_end +++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


      FUNCTION = 0                                    'success
      EXIT FUNCTION


    CASE %WM_SIZE

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_WM_Size +++++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_WM_Size +++++++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      FUNCTION = 0                                    'prevent DefFrameproc from sizing a MDI_Client to full client size
      EXIT FUNCTION


    CASE %WM_GetMinMaxInfo                            'override default size and position values
'      pminmax = lparam
'      @pminmax.ptMaxSize.x      =
'      @pminmax.ptMaxSize.y      =
'      @pminmax.ptMaxPosition.x  =
'      @pminmax.ptMaxPosition.y  =
'      @pminmax.ptMinTrackSize.x =
'      @pminmax.ptMinTrackSize.y =
'      @pminmax.ptMaxTrackSize.x =
'      @pminmax.ptMaxTrackSize.y =
'
'      FUNCTION = 0
'      EXIT FUNCTION


    CASE %WM_InitMenu                                 'main menu is about to be shown


    CASE %WM_NOTIFY
      pnmh = lParam
      SELECT CASE @pnmh.hwndfrom
      END SELECT                                      'end hwndfrom

      SELECT CASE @pnmh.code
        CASE %TTN_GETDISPINFO                         'get toolbar tooltip text
      END SELECT   


    CASE %WM_COMMAND
      SELECT CASE LO(WORD, wParam)
        CASE %Git_IDC_Btn_55                          'edit exclude list
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
'              if isfalse(isfile(cpath + ".gitignore")) then
'                open cpath + ".gitignore" for binary as #1
'                  put$ #1, "# list of files/filetypes to ignore in this worktree" + $crlf
'                  put$ #1, "Thumbs.db" + $crlf
'                  put$ #1, "Desktop.ini" + $crlf
'                  put$ #1, "_git_" + $crlf
'                  put$ #1, "*.tmp" + $crlf
'                  put$ #1, "*.tmc" + $crlf
'                  put$ #1, "*.tmi" + $crlf
'                  put$ #1, "*.log" + $crlf
'                  put$ #1, "*.bak" + $crlf
'                  put$ #1, "*.a" + $crlf
'                  put$ #1, "*.exe" + $crlf
'                  put$ #1, "*.dll" + $crlf
'                  put$ #1, "*.sll" + $crlf
'                close #1  
'
'              else
'                open cpath + ".gitignore" for binary as #1
'                  get$ #1, lof(1), d$
'                  replace $crlf with chr$(1) in d$
'                  replace $lf with chr$(1) in d$
'                  replace chr$(1) with $crlf in d$
'                  seek #1, 1
'                  put$ #1, d$
'                  seteof(1)
'                close #1  
'              end if
'
'              i = shell("notepad.exe " + $dq + cpath + ".gitignore" + $dq)


              if isfalse(isfile(cpath + ".git\info\exclude")) then
                open cpath + ".git\info\exclude" for binary as #1
                  put$ #1, "# list of files/filetypes to ignore in this worktree" + $crlf
                  put$ #1, "Thumbs.db" + $crlf
                  put$ #1, "Desktop.ini" + $crlf
                  put$ #1, "_git_" + $crlf
                  put$ #1, "*.tmp" + $crlf
                  put$ #1, "*.tmc" + $crlf
                  put$ #1, "*.tmi" + $crlf
                  put$ #1, "*.log" + $crlf
                  put$ #1, "*.bak" + $crlf
                  put$ #1, "*.a" + $crlf
                  put$ #1, "*.exe" + $crlf
                  put$ #1, "*.dll" + $crlf
                  put$ #1, "*.sll" + $crlf
                close #1  

              else
                open cpath + ".git\info\exclude" for binary as #1
                  get$ #1, lof(1), d$
                  replace $crlf with chr$(1) in d$
                  replace $lf with chr$(1) in d$
                  replace chr$(1) with $crlf in d$
                  seek #1, 1
                  put$ #1, d$
                  seteof(1)
                close #1  
              end if

              i = shell("notepad.exe " + $dq + cpath + ".git\info\exclude" + $dq)

              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_54                          'send mail  -> send patch dialog ???
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:sendmail /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_53                          'submodule sync
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:subsync /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_52                          'submodule update
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:subupdate /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_51                          'add to ignore list -> add file to ignore list
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file

              clipboard reset
              clipboard set text d$

'***********************************************************************************************
              if isfalse(isfile(cpath + ".gitignore")) then
                if msgbox(".gitignore not found, would you like to create it ?", _
                          %MB_yesno or %MB_Topmost or %MB_Applmodal or %MB_Iconquestion, _
                          ".gitignore") <> %IDYES then exit function

                open cpath + ".gitignore" for binary as #1
                close #1  
              end if

              i = shell("notepad.exe " + $dq + cpath + ".gitignore" + $dq)


              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function


'***********************************************************************************************

'              i = shell($dq + guipath + $dq + " /command:ignore /path:" + $dq + d$ + $dq)
'              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
'              settimer hwndm, 112, 100
'
'              rflag = 1                               'don�t reload
'              xflag = 1                               'let IDE wait
'              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
'              exit function
          END SELECT

        CASE %Git_IDC_Btn_50                          'blame
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:blame /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_49                          'delete (keep local)
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:remove /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_48                          'rename
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:rename /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_47                          'edit conflicts
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:conflicteditor /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_46                          'stash list (in reflog)
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:reflog /path:" + $dq + cpath + $dq + " /ref:refs/stash")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_45                          'stash pop
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:stashpop /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_44                          'stash apply
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:stashapply /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_43
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              ztext = "V " + FORMAT$(HI(BYTE, (HI(WORD, ver)))) + "." + _
                             FORMAT$(LO(BYTE, (HI(WORD, ver)))) + "." + _
                             FORMAT$(HI(BYTE, (LO(WORD, ver)))) + "." + _
                             FORMAT$(LO(BYTE, (LO(WORD, ver)))) + " - "

              ztext = ztext + "(" + date$ + " " + time$ + "):  standard commit"

              i = shell($dq + guipath + $dq + " /command:commit /path:" + $dq + cpath + $dq + " /logmsg:" + $dq + ztext + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_41                          'sync    
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:sync /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_40                          'clone
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
'              i = shell($dq + guipath + $dq + " /command:clone /exactpath /path:" + $dq + cpath + $dq)
              i = shell($dq + guipath + $dq + " /command:clone")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_39                          'about
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:about")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_38                          'help
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:help")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_37                          'settings
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:settings")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_36                          'apply patch  
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:importpatch") ' /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function

          END SELECT

        CASE %Git_IDC_Btn_35                          'create patch  
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:formatpatch /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_34                          'submodule add
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:subadd /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_33                          'add
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:add /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_32                          'export
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:export /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_31                          'create tag
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:tag /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_30                          'create branch  
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:branch /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_29                          'merge
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
'              i = shell($dq + guipath + $dq + " /command:merge/path:" + $dq + cpath + $dq)
              i = shell($dq + guipath + $dq + " /command:merge")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_28                          'switch/checkout
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:switch /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_27                          'cleanup
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:cleanup /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_26                          'revert
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:revert /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_25                          'resolve
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:resolve /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_24                          'bisect
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:bisect")
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

'              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_23                          'stash changes
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:stashsave /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_22                          'rebase
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:rebase /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_21                          'modifications
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:repostatus /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_20                          'browse
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:repobrowser /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_19                          'revision graph
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:revisiongraph /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_18                          'daemon
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:daemon /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_17                          'references
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:refbrowse /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_16                          'show ref log
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:reflog /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_15                          'show log
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:log /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_6                           'diff (previous)
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:prevdiff /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_5                           'diff
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              d$ = extract$(f$, "|")                  'current file
              i = shell($dq + guipath + $dq + " /command:diff /path:" + $dq + d$ + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_3                           'push
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:push /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_2                           'fetch
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:fetch") ' /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              rflag = 1                               'don�t reload
              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %Git_IDC_Btn_1                           'pull
          SELECT CASE HI(WORD, wParam)
            CASE %BN_CLICKED
              i = shell($dq + guipath + $dq + " /command:pull /path:" + $dq + cpath + $dq)
              hp = OpenProcess(%SYNCHRONIZE OR %PROCESS_QUERY_INFORMATION, %FALSE, i)
              settimer hwndm, 112, 100

              xflag = 1                               'let IDE wait
              postmessage hwnd, %WM_Syscommand, %SC_Close, 0
              exit function
          END SELECT

        CASE %IDOK
          messagebox 0, "<RETURN>", "%WM_Command", 0

        CASE %IDCANCEL
          postmessage hwnd, %WM_Syscommand, %SC_Close, 0
      END SELECT


    CASE %WM_PAINT


    CASE %WM_SYSCOLORCHANGE
      SendMessage Git_gethandle(%Git_IDC_Btn_1), %WM_SYSCOLORCHANGE, wparam, lparam '[ Pull...]
      SendMessage Git_gethandle(%Git_IDC_Btn_2), %WM_SYSCOLORCHANGE, wparam, lparam '[ Fetch...]
      SendMessage Git_gethandle(%Git_IDC_Btn_3), %WM_SYSCOLORCHANGE, wparam, lparam '[ Push...]
      SendMessage Git_gethandle(%Git_IDC_Fr_4), %WM_SYSCOLORCHANGE, wparam, lparam '[ Global ]
      SendMessage Git_gethandle(%Git_IDC_Btn_5), %WM_SYSCOLORCHANGE, wparam, lparam '[ Diff]
      SendMessage Git_gethandle(%Git_IDC_Btn_6), %WM_SYSCOLORCHANGE, wparam, lparam '[ Diff (previous)]
      SendMessage Git_gethandle(%Git_IDC_Fr_7), %WM_SYSCOLORCHANGE, wparam, lparam '[ Diff ]
      SendMessage Git_gethandle(%Git_IDC_Fr_8), %WM_SYSCOLORCHANGE, wparam, lparam '[ Log ]
      SendMessage Git_gethandle(%Git_IDC_Fr_9), %WM_SYSCOLORCHANGE, wparam, lparam '[ Bisect ]
      SendMessage Git_gethandle(%Git_IDC_Fr_10), %WM_SYSCOLORCHANGE, wparam, lparam '[ Clean ]
      SendMessage Git_gethandle(%Git_IDC_Fr_11), %WM_SYSCOLORCHANGE, wparam, lparam '[ Repository ]
      SendMessage Git_gethandle(%Git_IDC_Fr_12), %WM_SYSCOLORCHANGE, wparam, lparam '[ Add ]
      SendMessage Git_gethandle(%Git_IDC_Fr_13), %WM_SYSCOLORCHANGE, wparam, lparam '[ Patch ]
      SendMessage Git_gethandle(%Git_IDC_Fr_14), %WM_SYSCOLORCHANGE, wparam, lparam '[ Common ]
      SendMessage Git_gethandle(%Git_IDC_Btn_15), %WM_SYSCOLORCHANGE, wparam, lparam '[ Show Log]
      SendMessage Git_gethandle(%Git_IDC_Btn_16), %WM_SYSCOLORCHANGE, wparam, lparam '[ Show Ref Log]
      SendMessage Git_gethandle(%Git_IDC_Btn_17), %WM_SYSCOLORCHANGE, wparam, lparam '[ References]
      SendMessage Git_gethandle(%Git_IDC_Btn_18), %WM_SYSCOLORCHANGE, wparam, lparam '[ Daemon]
      SendMessage Git_gethandle(%Git_IDC_Btn_19), %WM_SYSCOLORCHANGE, wparam, lparam '[ Revision Graph]
      SendMessage Git_gethandle(%Git_IDC_Btn_20), %WM_SYSCOLORCHANGE, wparam, lparam '[ Browse Repo]
      SendMessage Git_gethandle(%Git_IDC_Btn_21), %WM_SYSCOLORCHANGE, wparam, lparam '[ Modifications]
      SendMessage Git_gethandle(%Git_IDC_Btn_22), %WM_SYSCOLORCHANGE, wparam, lparam '[ Rebase...]
      SendMessage Git_gethandle(%Git_IDC_Btn_23), %WM_SYSCOLORCHANGE, wparam, lparam '[ Stash Changes]
      SendMessage Git_gethandle(%Git_IDC_Btn_24), %WM_SYSCOLORCHANGE, wparam, lparam '[ Bisect]
      SendMessage Git_gethandle(%Git_IDC_Btn_25), %WM_SYSCOLORCHANGE, wparam, lparam '[ Resolve...]
      SendMessage Git_gethandle(%Git_IDC_Btn_26), %WM_SYSCOLORCHANGE, wparam, lparam '[ Revert...]
      SendMessage Git_gethandle(%Git_IDC_Btn_27), %WM_SYSCOLORCHANGE, wparam, lparam '[ Clean up...]
      SendMessage Git_gethandle(%Git_IDC_Btn_28), %WM_SYSCOLORCHANGE, wparam, lparam '[ Switch/Checkout...]
      SendMessage Git_gethandle(%Git_IDC_Btn_29), %WM_SYSCOLORCHANGE, wparam, lparam '[ Merge...]
      SendMessage Git_gethandle(%Git_IDC_Btn_30), %WM_SYSCOLORCHANGE, wparam, lparam '[ Create Branch...]
      SendMessage Git_gethandle(%Git_IDC_Btn_31), %WM_SYSCOLORCHANGE, wparam, lparam '[ Create Tag...]
      SendMessage Git_gethandle(%Git_IDC_Btn_32), %WM_SYSCOLORCHANGE, wparam, lparam '[ Export...]
      SendMessage Git_gethandle(%Git_IDC_Btn_33), %WM_SYSCOLORCHANGE, wparam, lparam '[ Add...]
      SendMessage Git_gethandle(%Git_IDC_Btn_34), %WM_SYSCOLORCHANGE, wparam, lparam '[ Submodule Add...]
      SendMessage Git_gethandle(%Git_IDC_Btn_35), %WM_SYSCOLORCHANGE, wparam, lparam '[ Create Patch...]
      SendMessage Git_gethandle(%Git_IDC_Btn_36), %WM_SYSCOLORCHANGE, wparam, lparam '[ Apply Patch...]
      SendMessage Git_gethandle(%Git_IDC_Btn_37), %WM_SYSCOLORCHANGE, wparam, lparam '[ Settings]
      SendMessage Git_gethandle(%Git_IDC_Btn_38), %WM_SYSCOLORCHANGE, wparam, lparam '[ Help]
      SendMessage Git_gethandle(%Git_IDC_Btn_39), %WM_SYSCOLORCHANGE, wparam, lparam '[ About]
      SendMessage Git_gethandle(%Git_IDC_Btn_40), %WM_SYSCOLORCHANGE, wparam, lparam '[ Clone...]
      SendMessage Git_gethandle(%Git_IDC_Btn_41), %WM_SYSCOLORCHANGE, wparam, lparam '[ Sync...]
      SendMessage Git_gethandle(%Git_IDC_Btn_43), %WM_SYSCOLORCHANGE, wparam, lparam '[ Commit...]
      SendMessage Git_gethandle(%Git_IDC_Btn_44), %WM_SYSCOLORCHANGE, wparam, lparam '[ Stash Apply]
      SendMessage Git_gethandle(%Git_IDC_Btn_45), %WM_SYSCOLORCHANGE, wparam, lparam '[ Stash Pop]
      SendMessage Git_gethandle(%Git_IDC_Btn_46), %WM_SYSCOLORCHANGE, wparam, lparam '[ Stash List]
      SendMessage Git_gethandle(%Git_IDC_Btn_47), %WM_SYSCOLORCHANGE, wparam, lparam '[ Edit Conflicts]
      SendMessage Git_gethandle(%Git_IDC_Btn_48), %WM_SYSCOLORCHANGE, wparam, lparam '[ Rename...]
      SendMessage Git_gethandle(%Git_IDC_Btn_49), %WM_SYSCOLORCHANGE, wparam, lparam '[ Delete]
      SendMessage Git_gethandle(%Git_IDC_Btn_50), %WM_SYSCOLORCHANGE, wparam, lparam '[ Blame]
      SendMessage Git_gethandle(%Git_IDC_Btn_51), %WM_SYSCOLORCHANGE, wparam, lparam '[ Add to Ignore List]
      SendMessage Git_gethandle(%Git_IDC_Btn_52), %WM_SYSCOLORCHANGE, wparam, lparam '[ Submodule Update]
      SendMessage Git_gethandle(%Git_IDC_Btn_53), %WM_SYSCOLORCHANGE, wparam, lparam '[ Submodule Sync]
      SendMessage Git_gethandle(%Git_IDC_Btn_54), %WM_SYSCOLORCHANGE, wparam, lparam '[ Send Mail...]
      SendMessage Git_gethandle(%Git_IDC_Btn_55), %WM_SYSCOLORCHANGE, wparam, lparam '[ Import Ignore List]


    CASE %WM_SYSCOMMAND
      SELECT CASE (wparam AND &HFFF0)
        CASE %SC_MINIMIZE

        CASE %SC_MAXIMIZE

        CASE %SC_RESTORE

        CASE %SC_KEYMENU

        CASE %SC_MOUSEMENU
      END SELECT


    CASE %WM_CLOSE


    CASE %WM_DESTROY                                  'exit - a good point for cleaning up
      FOR i = 1 TO UBOUND(ufont)
        deleteobject ufont(i)
      NEXT i

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_WM_Destroy ++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_WM_Destroy ++++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      if xflag = 0 then
ods("quit")
        PostQuitMessage 0                               'exit message loop in MAIN

      else
        xflag = 0
      end if

      hwndb = 0

      EXIT FUNCTION

  END SELECT


  IF hmdi then
    FUNCTION = DefFrameProc(hWnd, hmdi, wMsg, wParam, lParam)             'default processing (MDI)
  ELSE
    FUNCTION = DefWindowProc(hWnd, wMsg, wParam, lParam)                  'default processing
  END IF


END FUNCTION


'***********************************************************************************************


FUNCTION Git_Create_Window(BYVAL hparent AS DWORD) AS LONG
'***********************************************************************************************
' Register and create the main window
' You may add/insert own code and variables into the marked sections only, please keep variable names
'***********************************************************************************************
LOCAL uMsg        AS TAGMSG
LOCAL hinst       AS DWORD
LOCAL wce         AS WNDCLASSEX
#IF %DEF(%UNICODE)
LOCAL szClassName AS WSTRINGZ * 64
LOCAL szCaption   AS WSTRINGZ * 64
#ELSE
LOCAL szClassName AS ASCIIZ * 64
LOCAL szCaption   AS ASCIIZ * 64
#ENDIF
LOCAL hMenu       AS DWORD
LOCAL wstyle      AS DWORD
LOCAL xstyle      AS DWORD


  hinst = GetModuleHandle(byval 0)

  szClassName = UCS("BUTTONS")
  wce.cbSize = SIZEOF(wce)
  wce.STYLE = %CS_HREDRAW OR %CS_VREDRAW              'or %CS_DBLCLKS
  wce.lpfnWndProc = codeptr(Git_WindowProc)
  wce.hInstance = hInst
  wce.hCursor = LoadCursor(%NULL, BYVAL %IDC_ARROW)
  wce.hbrBackground = GetSyscolorbrush(%COLOR_3DFACE)
  wce.lpszClassName = VARPTR(szClassName)
  wce.hIcon = LoadIcon(hinst, "000_GIT_32_ICO")
  wce.hIconSm = LoadIcon(hinst, "GIT_16_ICO")


'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_Main_start ++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_Main_start ++++++++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


  RegisterClassEx wce


  szCaption = UCS(" Select Git Action ...")
  wstyle = %WS_POPUP OR %WS_BORDER OR %WS_DLGFRAME OR %WS_SYSMENU OR %WS_CLIPSIBLINGS OR %WS_CLIPCHILDREN OR %WS_VISIBLE OR %WS_CAPTION
  xstyle = %WS_EX_WINDOWEDGE OR %WS_EX_LEFT OR %WS_EX_LTRREADING OR %WS_EX_RIGHTSCROLLBAR
  hwndb = CreateWindowEx(xstyle, szClassName, szCaption, wstyle, 0 ,0, 0, 0, hparent, 0, hinst, byval 0)


  centerwindow(hwndb)


'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility begin_usercode_Main_before_show ++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

' insert your code here ...

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#utility end_usercode_Main_before_show ++++++++++++ (do not remove or edit this line!)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


  ShowWindow hwndb, %SW_SHOWDEFAULT                    'show main window
  UpdateWindow hwndb                                   'draw main window


END FUNCTION


'***********************************************************************************************
'***********************************************************************************************
'***********************************************************************************************
